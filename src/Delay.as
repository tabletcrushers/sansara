package
{
	public class Delay
	{
		private var delay:uint;
		private var cb:Function;
		
		public function update():void
		{
			delay--;
			
			if (delay == 0)
			{
				Game.game.delayVectorOn.removeAt(Game.game.delayVectorOn.indexOf(this));
				Game.game.delayVectorOff.push(this);
				cb();
			}
		}
		
		public function start(cb:Function, delay:uint):void
		{
			this.delay = delay;
			this.cb = cb;
			
			Game.game.delayVectorOn.push(this);
		}
	}
}