package 
{
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Polygon;
	import starling.display.Image;
	import starling.textures.Texture;
	import utils.Mut;
	import utils.Particles;
	
	public class Press extends ControlledObjPhys
	{
		private var cVelocity:Number;
		private var oVelocity:Number;
		private var extremePoint:Number;
		private var startPoint:Number;
		private var velocity:Number;
		private var fog:Particles;
		public var interruptMovement:Boolean;
		
		public function Press(body:Body, graphic:Image, initPoint:Number, deltaOpen:Number, closeVelocity:Number, openVelocity:Number, fog:Particles = null):void
		{
			super(body, graphic, initPoint);
			
			this.fog = fog;
			
			graphic.pivotX = graphic.width / 2;
			graphic.pivotY = graphic.height / 2;
			
			cVelocity = velocity = closeVelocity;
			oVelocity = openVelocity;
			
			extremePoint = body.position.y;
			body.position.y -= deltaOpen;
			startPoint = body.position.y;
			
			body.cbTypes.add(Game.game.CB_PRESS);
		}
		
		private function goToStart():void
		{
			velocity = oVelocity;
			Game.game.bumCameraEffect();
			if (fog != null && !interruptMovement) 
			{
				if (fog.parent == null) Game.game.layer3.addChild(fog);
				fog.startPS(body.position.x, body.position.y + graphic.height / 2 - 21, 210);
			}
			interruptMovement = false;
		}
		
		override protected function action():void
		{
			if (Math.abs(body.position.x - Game.game.hero.center.x) > 210) 
			{
				if ((cVelocity > 0 && body.position.y <= startPoint) || (cVelocity < 0 && body.position.y >= startPoint)) 
				return;
			}
			
			if (cVelocity > 0)
			{
				if ((body.position.y >= extremePoint && velocity == cVelocity) || interruptMovement) goToStart();
				else if (body.position.y <= startPoint && velocity == oVelocity) velocity = cVelocity;
			}
			else
			{
				if ((body.position.y <= extremePoint && velocity == cVelocity) || interruptMovement) goToStart();
				else if (body.position.y >= startPoint && velocity == oVelocity) velocity = cVelocity;
			}
			
			body.position.y += velocity;
		}
	}
	
}