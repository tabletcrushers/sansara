package 
{
	public class ControlledObj
	{
		protected var inited:Boolean;
		protected var initPoint:Number;
		
		public function ControlledObj(ip:Number)
		{
			initPoint = ip;
		}
		
		public function enterFrame():void
		{
			if (!inited)
			{
				if (initCondition()) 
				{
					init();
					inited = true;
				}
				return;
			}
			
			action();
		}
		
		protected function initCondition():Boolean
		{
			if (Game.game.borderRight >= initPoint) return true;
			return false;
		}
		
		protected function init():void
		{
			
		}
		
		protected function action():void
		{
			
		}
	}
	
}