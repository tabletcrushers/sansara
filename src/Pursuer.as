package 
{
	//import com.eclecticdesignstudio.motion.Actuate;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import utils.Mut;
	import starling.display.DisplayObject;
	/*import nape.geom.Ray;
	import nape.geom.RayResult;
	import utils.Particles;*/
	
	public class Pursuer extends ControlledObjPhys
	{
		private var velocity:Number;
		private var angleToTarget:Number;
		private var uVec2:Vec2 = Vec2.get();
		private var initPointV:Vec2 = Vec2.get();
		private var maxDistance:uint;
		
		public function Pursuer(body:Body, graphic:DisplayObject, initPoint:Vec2, vel:uint, maxD:uint = 0)
		{
			super(body, graphic, initPoint.x);
			
			maxDistance = maxD;
			velocity = vel;
			initPointV.set(initPoint);
			
			body.gravMass = 0;
			//body.shapes.at(0).sensorEnabled = true;
			body.cbTypes.add(Game.game.CB_FATAL);
			
			body.userData.parent = this;
		}
		
		override protected function action():void
		{
			if (Mut.distV(body.position, initPointV) > maxDistance)
			{
				clear();
				return;
			}
			
			angleToTarget = Mut.getAng(body.position, Game.game.hero.center);
			
			body.velocity.setxy(velocity * Math.cos(angleToTarget), velocity * Math.sin(angleToTarget));
			
			//Particles(graphic).startPS(body.position.x, body.position.y);
		}
		
		public function attack():void
		{
			if (!inited) return;
			
			Game.game.hero.dying();
			Game.game.gameOver();
		}
	}
	
}