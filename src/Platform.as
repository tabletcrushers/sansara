package 
{
	import com.eclecticdesignstudio.motion.Actuate;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Polygon;
	import starling.display.Image;
	import starling.textures.Texture;
	import utils.Mut;
	import utils.Particles;
	
	public class Platform extends ControlledObjPhys
	{
		private var pathTime:Number;
		private var pathLength:Number;
		
		public function Platform(body:Body, graphic:Image, initPoint:Number, pathLength:Number, pathTime:Number, delay:Number):void
		{
			super(body, graphic, initPoint);
			
			graphic.pivotX = graphic.width / 2;
			graphic.pivotY = graphic.height / 2;
			
			this.pathTime = pathTime;
			this.pathLength = pathLength;
			
			//body.cbTypes.add(Game.game.CB_PRESS);
		}
		
		private function moveToTarget(target:Number):void
		{
			Actuate.tween(body.position, pathTime, {x:target}).onComplete(function():void
			{
				if (body.position.x == initPoint)
				{
					moveToTarget(initPoint + pathLength);
				}
				else
				{
					moveToTarget(initPoint);
				}
			}
		}
		
		override protected function additionalInit():void
		{
			moveToTarget(initPoint + pathLength);
		}
		
		override protected function additionalClear():void
		{
			body
		}
	}
	
}