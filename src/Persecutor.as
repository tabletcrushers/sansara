package 
{
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Cubic;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import utils.Mut;
	import starling.display.DisplayObject;
	import nape.geom.Ray;
	import nape.geom.RayResult;
	import utils.Particles;
	
	public class Persecutor extends ControlledObjPhys
	{
		public var pathMarkers:Vector.<Vec2>;
		private var currentVel:Number = 280;
		private var angleToTarget:Number;
		private var uVec2:Vec2 = Vec2.get();
		private var particles:Particles;
		private var attacking:Boolean = false;
		
		public function Persecutor(body:Body, initPoint:Number, vel:uint = 280, graphic:DisplayObject = null, part:Particles = null)
		{
			super(body, graphic, initPoint);
			
			body.gravMass = 0;
			body.shapes.at(0).sensorEnabled = true;
			body.cbTypes.add(Game.game.CB_PERSECUTOR);
			pathMarkers = new Vector.<Vec2>();
			
			currentVel = vel;
			
		}
		
		override protected function action():void
		{
			if (Game.game.hero.isDead)
			{
				return;
			}
			
			if (pathMarkers.length == 0)
			{
				clear();
				return;
			}
			
			if (!attacking)
			{
				angleToTarget = Mut.getAng(body.position, pathMarkers[0]);
			}
			else
			{
				angleToTarget = Mut.getAng(body.position, Game.game.hero.center);
				body.userData.vel += 2;
				Game.game.hero.slowDown();
				
				if (Mut.distV(body.position, Game.game.hero.center) < 40)
				{
					body.velocity.setxy(0, 0);
					Game.game.gameOver(false, 1);
					Actuate.tween(body, 1, {rotation:0});
					return;
				}
			}
			
			//body.rotation = Mut.lerp(body.rotation, angleToTarget, .04);
			
			body.rotation += (angleToTarget - body.rotation) / 7;
			
			body.velocity.setxy(body.userData.vel * Math.cos(body.rotation), body.userData.vel * Math.sin(body.rotation));
			
			if (Math.abs(body.rotation) > Math.PI / 2 && graphic.scaleY > 0)
			{
				//graphic.scaleY = -1
			}
			else if (Math.abs(body.rotation) < Math.PI / 2 && graphic.scaleY < 0)
			{
				//graphic.scaleY = 1
			}
			
			if (particles != null)
			{
				particles.startPS(body.position.x, body.position.y);
			}
			
			if (attacking)
			{
				return;
			}
			
			if (Mut.distV(body.position, pathMarkers[0]) < currentVel / 4)
			{
				pathMarkers.shift();
				Actuate.tween(body.userData, 2, { vel:currentVel } ).ease(Cubic.easeIn);
			}
			else if (Mut.distV(body.position, pathMarkers[0]) < currentVel / 2)
			{
				Actuate.stop(body.userData);
				
				if (body.userData.vel > 70)
				{
					body.userData.vel *= .99;
				}
			}
			
		}
		
		override protected function additionalInit():void
		{
			body.rotation = angleToTarget = Mut.getAng(body.position, pathMarkers[0]);
			body.userData.vel = currentVel;
			
			if (graphic is VibroGraphic)
			{
				VibroGraphic(graphic).init();
			}
		}
		
		override protected function initCondition():Boolean
		{
			if (Game.game.hero.center.x >= initPoint)
			{
				return true;
			}
			return false;
		}
		
		public function attack():void
		{
			if (!inited || attacking) 
			{
				return;
			}
			
			attacking = true;
			
			/*body.rotation = Mut.getAng(body.position, Game.game.hero.center);
			uVec2.set(body.position);
			uRay.origin.set(uVec2);
			uRay.maxDistance = 210;
			uRay.direction.setxy(Math.cos(body.rotation), Math.sin(body.rotation));
			var res:RayResult = Game.game.space.rayCast(uRay);
			if (res == null || res.shape.body.cbTypes.length < 1 || res.shape.body.cbTypes.at(1) != Game.game.CB_HERO) return;
			
			trace("booommmmmmmmmmmm!!!!!!!!!!!!!!!!!!!");*/
		}
		
	}
	
}