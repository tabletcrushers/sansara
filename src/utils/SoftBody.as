package utils 
{
    import com.adobe.utils.AGALMiniAssembler;
	import flash.system.System;
	import flash.utils.ByteArray;
	import starling.textures.Texture;
	import nape.geom.GeomPoly;
	import nape.geom.Vec2;
    
    import flash.display3D.*;
    import flash.geom.*;
    
    import starling.core.RenderSupport;
    import starling.core.Starling;
    import starling.display.DisplayObject;
    import starling.errors.MissingContextError;
    import starling.events.Event;
    import starling.utils.VertexData;
    
    /** This custom display objects renders a regular, n-sided polygon. */
    public class SoftBody extends DisplayObject
    {
		public var noRender:Boolean = false;
		
		private static const PROGRAM_NAME:String = "polygonBody";
		private var texture:Texture;
		public var numSegments:uint;
		private var vertexData:VertexData;
        private var indexData:Vector.<uint>;
		
		private var vertexBuffer:VertexBuffer3D;
        private var indexBuffer:IndexBuffer3D;
		
		public var softPolygon:SoftPolygon;
		public var defaultBody:SoftPolygon;
		public var ball:SoftPolygon;
		
		public var center:Vec2 = new Vec2();
		
		private var boundsRect:Rectangle = new Rectangle();
        
        public function SoftBody(defaultBody:SoftPolygon, ball:SoftPolygon, texture:Texture)
        {
            super();
			
			this.defaultBody = defaultBody;
			this.ball = ball;
			softPolygon = defaultBody;
			numSegments = softPolygon.body.bodies.length;
			
			this.texture = texture;
			
			vertexData = new VertexData(numSegments + 1);
            indexData  = new Vector.<uint>(3 * numSegments);
			//indexData  = new Vector.<uint>();
			
			setupGeometry(true);
			
			createBuffers();
            registerPrograms();
        }
		
		public function replaceBody(isDefaultBody:Boolean = true):void
		{
			Game.game.softBodies.removeAt(Game.game.softBodies.indexOf(softPolygon));
			
			if (isDefaultBody)
			{
				softPolygon = defaultBody;
			}
			else
			{
				softPolygon = ball;
			}
			
			Game.game.softBodies.push(softPolygon);
			
			//setupGeometry(true);
			
			//createBuffers();
            //registerPrograms();
		}
        
        /** Disposes all resources of the display object. */
        public override function dispose():void
        {
            Starling.current.removeEventListener(Event.CONTEXT3D_CREATE, onContextCreated);
            
            if (vertexBuffer) vertexBuffer.dispose();
            if (vertexBuffer) vertexBuffer.dispose();
			
			texture.dispose();
			softPolygon.dispose();
			
			boundsRect = null;
			
			vertexData = null;
            
            super.dispose();
        }
        
        private function onContextCreated(event:Event):void
        {
            // the old context was lost, so we create new buffers and shaders.
            createBuffers();
            registerPrograms();
        }
        
        /** Returns a rectangle that completely encloses the object as it appears in another 
         * coordinate system. */
        public override function getBounds(targetSpace:DisplayObject, resultRect:Rectangle=null):Rectangle
        {
            /*if (resultRect == null) resultRect = new Rectangle();
            
            var transformationMatrix:Matrix = targetSpace == this ? 
                null : getTransformationMatrix(targetSpace, sHelperMatrix);*/
            
            return boundsRect;
        }
        
        private function setupGeometry(setIndices:Boolean = false):void 
		{
			center.setxy(0, 0);
			//center = softPolygon.body.COM();
			
			for (var sideVertexI:uint = 0; sideVertexI < numSegments; ++sideVertexI) 
			{
                var angle:Number = 2 * Math.PI * sideVertexI / numSegments;
				
				//var angle=Mut.getAng(center, vertices[sideVertexI]);
				var sinA:Number = Math.sin(angle);
				var cosA:Number = Math.cos(angle);
				
				vertexData.setPosition(sideVertexI, softPolygon.vertices[sideVertexI].x, softPolygon.vertices[sideVertexI].y);
				vertexData.setTexCoords(sideVertexI, .5 + .5 * cosA, .5 + .5 * sinA);
				
				if (setIndices)
				{
					for (var i:uint = 0; i < numSegments; ++i) indexData.push(numSegments, i, (i + 1) % numSegments);;
				}
				
				center.x += softPolygon.vertices[sideVertexI].x;
				center.y += softPolygon.vertices[sideVertexI].y;
            }
			
			center.x /= numSegments;
			center.y /= numSegments;
			
			vertexData.setPosition(numSegments, center.x, center.y);
			vertexData.setTexCoords(numSegments, .5, .5);
        }
		
		private function createBuffers():void {
            var context:Context3D = Starling.context;
            if (context == null) throw new MissingContextError();
			
			if(vertexBuffer) vertexBuffer.dispose();
            if(indexBuffer) indexBuffer.dispose();
			
			const verticesCount:uint = numSegments + 1;
			
			vertexBuffer = context.createVertexBuffer(verticesCount, VertexData.ELEMENTS_PER_VERTEX);
            vertexBuffer.uploadFromVector(vertexData.rawData, 0, vertexData.numVertices);
			
			const indicesCount:uint = 3 * numSegments;
			
			indexBuffer = context.createIndexBuffer(indexData.length);
            indexBuffer.uploadFromVector(indexData, 0, indexData.length);
        }
        
        private function registerPrograms():void {
            var starling:Starling = Starling.current;
            if (starling.hasProgram(PROGRAM_NAME)) return;
			
			var vertexAGAL:String =
                "m44 op, va0, vc0 \n" +
                "mov v0, va1";
				
			var fragmentAGAL:String =
                "tex oc, v0, fs0 <2d, clamp, linear, mipnone> \n"; // just sample texture color
			
			var asm:AGALMiniAssembler      = new AGALMiniAssembler(),
                vertexBytecode:ByteArray   = asm.assemble(Context3DProgramType.VERTEX,   vertexAGAL),
                fragmentBytecode:ByteArray = asm.assemble(Context3DProgramType.FRAGMENT, fragmentAGAL);
				
			starling.registerProgram(PROGRAM_NAME, vertexBytecode, fragmentBytecode);
        }
		
		override public function render(support:RenderSupport, parentAlpha:Number):void 
		{
            if (noRender) return;
			var context:Context3D = Starling.context;
            if (context == null) throw new MissingContextError();
			support.finishQuadBatch();
			
			setupGeometry();
			
			vertexBuffer.uploadFromVector(vertexData.rawData, 0, numSegments + 1);
			
			//support.applyBlendMode(texture.premultipliedAlpha);
			support.applyBlendMode(false);
            context.setProgram(Starling.current.getProgram(PROGRAM_NAME));
			
			context.setVertexBufferAt(0, vertexBuffer, VertexData.POSITION_OFFSET, Context3DVertexBufferFormat.FLOAT_2);
            context.setVertexBufferAt(1, vertexBuffer, VertexData.TEXCOORD_OFFSET, Context3DVertexBufferFormat.FLOAT_2);
            context.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, support.mvpMatrix3D, true); // mvp matrix, vc0-vc3
			
			context.setTextureAt(0, texture.base);
			
			context.drawTriangles(indexBuffer);
			
			context.setVertexBufferAt(0, null);
            context.setVertexBufferAt(1, null);
            context.setTextureAt(0, null);
        }
    }
}