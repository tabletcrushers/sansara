package utils
{
	import flash.display.Bitmap;
    import flash.ui.Keyboard;
    import flash.utils.setTimeout;
	
    import starling.core.Starling;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.events.KeyboardEvent;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.extensions.PDParticleSystem;
    import starling.extensions.ParticleSystem;
    import starling.textures.Texture;
	import starling.textures.SubTexture;
    
    public class Particles extends Sprite
    {
        
        
        // member variables
		
		private var Config:Class;
		private var ParticleTexture:Class;
        
        public var mParticleSystem:ParticleSystem;
		private var texture:Texture;
		private var config:XML;
        
        public function Particles(Cnf:Class, Prt:Class)
        {
			super();
			
			Config = Cnf;
			ParticleTexture = Prt;
        }
		
		public function init():void
        {
            config = XML(new Config());
            texture = Texture.fromBitmap(new ParticleTexture());
			mParticleSystem = new PDParticleSystem(config, texture);
        }
        
        public function startPS(x:Number, y:Number, time:Number = 0, time1:Number = 0):void
        {
            mParticleSystem.emitterX = x;
            mParticleSystem.emitterY = y;
            mParticleSystem.start();
            
            addChild(mParticleSystem);
            Starling.juggler.add(mParticleSystem);
			
			if (time != 0)
			{
				setTimeout(
					function():void
					{ 
						stopPS(time1);
					}, time
				);
			}
        }
        
        public function stopPS(time:uint = 0):void
        {
            mParticleSystem.stop();
			if(time != 0) setTimeout(function():void
			{
				Starling.juggler.remove(mParticleSystem);
				removeChild(mParticleSystem);
				this.removeFromParent(true);
			}, time);
			
        }
    }
}