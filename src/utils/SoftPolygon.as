package utils
{
	import flash.display.Shape;
	import nape.constraint.Constraint;
	import nape.constraint.ConstraintList;
	import nape.dynamics.InteractionGroup;
	import nape.geom.GeomPoly;
	import nape.geom.Vec2;
	import nape.phys.Compound;
	import nape.phys.Body;
	import nape.shape.Edge;
	import nape.shape.Polygon;
	import nape.constraint.PivotJoint;
	
	public class SoftPolygon
	{
		public var body:Compound;
		public var vertices:Vector.<Vec2>;
		public var elasticJoints:ConstraintList = new ConstraintList();
		private var frequency:Number;
		private var damping:Number;
		
		private var heroGroup:InteractionGroup = new InteractionGroup(true);
		
		public function SoftPolygon(poly:GeomPoly, position:Vec2, thickness:Number, discretisation:Number, frequency:Number, damping:Number)
		{
            // We're going to collect all Bodies and Constraints into a Compound
            // for ease of use hereafter.
            body = new Compound();
			
			this.frequency = frequency;
			this.damping = damping;
			
			// Lists of segments, and the outer and inner points for joint formations.
            var segments:Vector.<Body> = new Vector.<Body>();
            var outerPoints:Vector.<Vec2> = new Vector.<Vec2>();
            var innerPoints:Vector.<Vec2> = new Vector.<Vec2>();
			
			// Set of Edge references to vertex of segments to be used in drawing bodies
            // and in determining area for gas forces.
            var refEdges:Vector.<Edge> = new Vector.<Edge>();
			body.userData.refEdges = refEdges;
			
			// Deflate the input polygon for inner vertices
            var inner:GeomPoly = poly.inflate( -thickness);
			
			// Create Bodies about the border of polygon.
            var start:Vec2 = poly.current();
            var i:uint, current:Vec2, iCurrent:Vec2;
			
			do {
                // Current and next vertex along polygon.
                current = poly.current();
				
                poly.skipForward(1);
                var next:Vec2 = poly.current();
				
				// Current and next vertex along inner-polygon.
                iCurrent = inner.current();
                inner.skipForward(1);
                var iNext:Vec2 = inner.current();
				
				var delta:Vec2 = next.sub(current);
                var iDelta:Vec2 = iNext.sub(iCurrent);
				
				var length:Number = Math.max(delta.length, iDelta.length);
                var numSegments:int = Math.ceil(length / discretisation);
                var gap:Number = (1 / numSegments);
				
				for (i = 0; i < numSegments; i++) {
                    // Create softBody segment.
                    // We are careful to create weak Vec2 for the polygon
                    // vertices so that all Vec2 are automatically released
                    // to object pool.
                    var segment:Body = new Body();
					
					var outerPoint:Vec2 = current.addMul(delta, gap * i);
                    var innerPoint:Vec2 = iCurrent.addMul(iDelta, gap * i);
					
                    var polygon:Polygon = new Polygon([outerPoint,  current.addMul(delta, gap * (i + 1), true), iCurrent.addMul(iDelta, gap * (i + 1), true), innerPoint]); 
					
					
                    polygon.body = segment;
					segment.compound = body;
                    segment.align();
					
					segments.push(segment);
                    outerPoints.push(outerPoint);
                    innerPoints.push(innerPoint);
					
					refEdges.push(polygon.edges.at(0));
                }
				// Release vectors to object pool.
                delta.dispose();
                iDelta.dispose();
            }
            while (poly.current() != start);
			
			// Create sets of PivotJoints to link segments together.
            for (i = 0; i < segments.length; i++) {
                var leftSegment:Body = segments[(i - 1 + segments.length) % segments.length];
                var rightSegment:Body = segments[i];
				
				rightSegment.group = leftSegment.group = heroGroup;
				
				// We create a stiff PivotJoint for outer edge
                current = outerPoints[i];
                var pivot:PivotJoint = new PivotJoint(leftSegment, rightSegment, leftSegment.worldPointToLocal(current, true), rightSegment.worldPointToLocal(current, true));
                current.dispose();
                pivot.compound = body;
				
				// And an elastic one for inner edge
                current = innerPoints[i];
                pivot = new PivotJoint(leftSegment, rightSegment, leftSegment.worldPointToLocal(current, true), rightSegment.worldPointToLocal(current, true));
                current.dispose();
                pivot.compound = body;
                pivot.stiff = false;
                pivot.frequency = frequency;
                pivot.damping = damping;
				elasticJoints.push(pivot);
			   
			   //pivot.frequency = 10000;
               // pivot.damping = 7;
				
				// And set one of them to have 'ignore = true' so that
                // adjacent segments do not interact.
                pivot.ignore = true;
            }
			
			// Release vertices of inner polygon to object pool.
            inner.clear();
			
			// Move segments by required offset
            for (i = 0; i < segments.length; i++)  segments[i].position.addeq(position);
			
			body.userData.area = polygonalArea(body);
			
			vertices = new Vector.<Vec2>(outerPoint.length);
			for (i = 0; i < body.bodies.length; i++) vertices[i] = body.bodies.at(i).shapes.at(0).castPolygon.worldVerts.at(0);
        }
		
		public function changeStiffness(state:Boolean = false):void
		{
			elasticJoints.foreach(function(j:PivotJoint):void
			{
				j.stiff = state;
			});
		}
		
		/*public function changeFrequency(frq:Number = 0):void
		{
			if (frq == 0)
			{
				frq = frequency;
			}
			
			elasticJoints.foreach(function(j:PivotJoint)
			{
				j.frequency = frq;
			});
		}
		
		public function changeDamping(damp:Number = 0):void
		{
			if (damp == 0)
			{
				damp = damping;
			}
			
			elasticJoints.foreach(function(j:PivotJoint)
			{
				j.damping = damp;
			});
		}*/
		
		public function dispose():void
		{
			for each(var i:Vec2 in vertices) i.dispose();
			body.visitBodies(function(b:Body):void
			{
				for each(var shp:Shape in b.shapes) shp = null;
				for each(var c:Constraint in b.constraints)
				{
					c.space = null;
					c = null;
				}
				b.space = null;
				b = null;
			});
		}
		
		private var areaPoly:GeomPoly = new GeomPoly();
		public function polygonalArea(s:Compound):Number 
		{
            // Computing the area of the soft body, we use the vertices of its edges
            // to populate a GeomPoly and use its area function.
            var refEdges:Vector.<Edge> = s.userData.refEdges;
            for (var i:uint = 0; i < refEdges.length; i++) 
			{
                var edge:Edge = refEdges[i];
                areaPoly.push(edge.worldVertex1);
            }
            var ret:Number = areaPoly.area();
			
			// We use the same GeomPoly object, and recycle the vertices
            // to avoid increasing memory.
            areaPoly.clear();
			
			return ret;
        }
	}
	
}