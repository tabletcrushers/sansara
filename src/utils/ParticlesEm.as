package utils
{
	import starling.display.Sprite;
	import starling.textures.Texture;

	public class ParticlesEm
	{
		public var toRemove:Boolean;
		
		private var rStartX:int = 0;
		private var rStartY:int = 0;
		private var startRadius:int = 0;
		
		private var numParticles:int = 0;
		private var life:int = 0;
		private var randomLife:int = 0;
		
		private var interTime:int = 0;
		private var randomInterTime:int = 0;
		
		private var vel:int = 0;
		private var randomVel:int = 0;
		private var speedUp:int = 0;
		private var allowVelNegative:int = 0;
		
		private var angle:Number = 0;
		private var spreadAngle:Number = 0;
		
		private var sScale:int = 100;
		private var randomScaleStart:int = 0;
		private var mScale:int = 100;
		private var randomScaleMiddle:int = 0;
		private var fScale:int = 100;
		private var randomScaleFinish:int = 0;
		
		private var rotV:Number = 0;
		private var randomRotDir:int = 0;
		private var randomRot:Number = 0;
		private var randomRotStart:int = 0;
		private var setRotByDir:int = 0;
		
		private var sa:int = 100;
		private var ma:int = 100;
		private var fa:int = 100;
		
		private var randomGravY:Number = 0;
		
		private var gravY:Number = 0;
		private var gravX:Number = 0;
		private var gravA:Number = 0;
		private var gravAX:Number = 0;
		
		private var delay:int = 0;
		private var del:int = 0;
		
		private var flickerMax:int = 0;
		private var flickerMin:int = 0;
		
		
		
		
		public var particles:Vector.<Particle>;
		public var texture:Texture;
		public var eTimes:int = 0;
		private var x:Number = 0;
		private var y:Number = 0;
		
		private var parent:Sprite;
		
		
		public function ParticlesEm (texture:Texture, xmlS:String, prnt:Sprite, numPar:uint = 0):void
		{
			this.texture = texture;
			
			parent = prnt;
			
			particles = new Vector.<Particle>;
			
			var txml:XML = new XML(xmlS);
			
			if (txml.child("RandomStartX").length() > 0) rStartX = int(txml.child("RandomStartX"));
			if (txml.child("RandomStartY").length() > 0) rStartY = int(txml.child("RandomStartY"));
			if (txml.child("StartRadius").length() > 0) startRadius = int(txml.child("StartRadius"));
			if (txml.child("ParticlesPerIteration").length() > 0) numParticles = int(txml.child("ParticlesPerIteration"));
			if (txml.child("Life").length() > 0) life = int(txml.child("Life"));
			if (txml.child("RandomLife").length() > 0) randomLife = int(txml.child("RandomLife"));
			if (txml.child("IntermediateTime").length() > 0) interTime = int(txml.child("IntermediateTime"));
			if (txml.child("RandomInterTime").length() > 0) randomInterTime = int(txml.child("RandomInterTime"));
			if (txml.child("Velocity").length() > 0) vel = int(txml.child("Velocity"));
			if (txml.child("RandomizeVelocity").length() > 0) randomVel = int(txml.child("RandomizeVelocity"));
			if (txml.child("SpeedUp").length() > 0) speedUp = int(txml.child("SpeedUp"));
			if (txml.child("AllowVelNegative").length() > 0) allowVelNegative = int(txml.child("AllowVelNegative"));
			if (txml.child("Direction").length() > 0) angle = int(txml.child("Direction")) * Math.PI / 180;
			if (txml.child("SpreadDirection").length() > 0) spreadAngle = int(txml.child("SpreadDirection")) * Math.PI / 180;
			if (txml.child("StartScale").length() > 0) sScale = int(txml.child("StartScale"));
			if (txml.child("RandomScaleStart").length() > 0) randomScaleStart = int(txml.child("RandomScaleStart"));
			if (txml.child("IntermediateScale").length() > 0) mScale = int(txml.child("IntermediateScale"));
			if (txml.child("RandomScaleIntermediate").length() > 0) randomScaleMiddle = int(txml.child("RandomScaleIntermediate"));
			if (txml.child("FinishScale").length() > 0) fScale = int(txml.child("FinishScale"));
			if (txml.child("RandomScaleFinish").length() > 0) randomScaleFinish = int(txml.child("RandomScaleFinish"));
			if (txml.child("RotationVel").length() > 0) rotV = int(txml.child("RotationVel")) / 200;
			if (txml.child("RandomRotDir").length() > 0) randomRotDir = int(txml.child("RandomRotDir"));
			if (txml.child("RandomizeRotVel").length() > 0) randomRot = int(txml.child("RandomizeRotVel")) / 200;
			if (txml.child("RandomRotStart").length() > 0) randomRotStart = int(txml.child("RandomRotStart"));
			if (txml.child("SetRotByDir").length() > 0) setRotByDir = int(txml.child("SetRotByDir"));
			if (txml.child("StartAlpha").length() > 0) sa = int(txml.child("StartAlpha"));
			if (txml.child("IntermediateAlpha").length() > 0) ma = int(txml.child("IntermediateAlpha"));
			if (txml.child("FinishAlpha").length() > 0) fa = int(txml.child("FinishAlpha"));
			if (txml.child("RandomGravY").length() > 0) randomGravY = int(txml.child("RandomGravY"));
			if (txml.child("GravityY").length() > 0) gravY = int(txml.child("GravityY"));
			if (txml.child("GravityX").length() > 0) gravX = int(txml.child("GravityX"));
			if (txml.child("GravityAY").length() > 0) gravA = int(txml.child("GravityAY"));
			if (txml.child("GravityAX").length() > 0) gravAX = int(txml.child("GravityAX"));
			if (txml.child("Delay").length() > 0) delay = int(txml.child("Delay"));
			if (txml.child("FlickerMax").length() > 0) flickerMax = int(txml.child("FlickerMax"));
			if (txml.child("FlickerMin").length() > 0) flickerMin = int(txml.child("FlickerMin"));
			
			if (delay != 0) del = 1;
			
			if (numPar != 0) numParticles = numPar;
		}
		public function emitStart(x:Number, y:Number, times:int):void
		{
			this.x = x;
			this.y = y;
			eTimes = times;
			if (times <= delay) throw(" class: ParticlesEm; function: emitStart >>> times should be > delay tag value defined in xml. ptexture = " + texture);
		}
		
		public function emitStop():void
		{
			eTimes = 0;
		}
		
		public function emit(x:Number, y:Number):void
		{
			if (eTimes == 0) return;
			if (eTimes < 7777777) eTimes--;
			if (delay != 0)
			{
				if (del != 0) 
				{
					del --;
					return;
				}
				else del = delay;
			}
			var rotZnak:int = 1;
			
			for (var i:uint = 0; i < numParticles; i++)
			{
				if (randomRotDir != 0) rotZnak *= -1;
				
				var pLife:int = Math.ceil(life + randomLife * Math.random());
				var a:Number = angle + spreadAngle - 2 * Math.random() * spreadAngle;
				var v:Number = vel/20 + randomVel * Math.random() / 20;
				var rv:Number = rotV * rotZnak + randomRot - 2 * Math.random() * randomRot;
				
				//if (interTime + randomInterTime > 99) throw(" emit 145: interTime + randomInterTime should be < 100");
				
				var it:int = interTime + Math.ceil(randomInterTime * Math.random());
				
				var particle:Particle = new Particle(texture, v, speedUp, a, allowVelNegative, pLife, it, sScale + randomScaleStart * Math.random(), 
				mScale + randomScaleMiddle * Math.random(), fScale + randomScaleFinish * Math.random(),
				rv, sa, ma, fa, gravX / 100, (gravY + randomGravY / 2 - randomGravY * Math.random()) / 100, gravA / 100, gravAX / 100, flickerMax, flickerMin);
				
				particle.x = x + rStartX - rStartX * 2 * Math.random() + Math.cos(a) * startRadius;
				particle.y = y + rStartY - rStartY * 2 * Math.random() + Math.sin(a) * startRadius;
				if (randomRotStart != 0 && setRotByDir != 0) throw(" emit 139: randomRotStart, setRotByDir - one of them should be 0");
				if (randomRotStart != 0) particle.rotation = Math.random() * randomRotStart * Math.PI / 180
				else if (setRotByDir != 0) { particle.rotation = a;}
				parent.addChild(particle);
				particles.push(particle);
			}
		}
		
		public function onEnterFrame():void
		{
			if (eTimes != 0)
			{
				emit(x, y);
			}
			
			for (var i:uint = 0; i < particles.length; i++)
			{
				var p:Particle = particles[i];
				
				if (p != null)
				{
					
					if (p.isLive < 1) 
					{
						particles.splice(particles.indexOf(p), 1);
						parent.removeChild(p);
						
						continue;
					}
					p.live();
				}
			}
		}
	}
}