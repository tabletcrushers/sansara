package utils
{
	public class Oscillator
	{
		public var time:Number = 0;
		public var startValue:Number = 0;
		public var amplitude:Number = 50;
		public var period:Number = 2;
		public var angularFreqNatural:Number = Math.PI / period;
		public var gamma:Number = .2;
		public var angularFreq:Number = Math.sqrt(Math.pow(angularFreqNatural, 2) - Math.pow(gamma / 2, 2));
		public var val:Number;
		public var timeStep:Number = 1 / 10;
		public var paused:Boolean = true;
		public var timeLim:Number = 10;
		private var obj:Object;
		private var property:String;
		public var isActive:Boolean = false;
		private var completeHandler:Function;
		private var completeObject:Object;
		
		public function Oscillator(cObj:Object = null, completeHandler:Function = null)
		{
			this.completeHandler = completeHandler;
			this.completeObject = cObj;
		}
		
		public function start():Oscillator
		{
			time = 0;
			Game.game.oscilators.push(this);
			isActive = true;
			
			return this;
		}
		
		public function setup(obj:Object, property:String, timeStep:Number = 1/10, startValue:Number = 0, amplitude:Number = 50, period:Number = 2, gamma:Number = .2):void
		{
			this.obj = obj;
			this.property = property;
			this.timeLim = timeLim;
			this.timeStep = timeStep;
			this.startValue = startValue;
			this.amplitude = amplitude;
			this.period = period;
			this.gamma = gamma;
			
			angularFreqNatural = Math.PI / period;
			angularFreq = Math.sqrt(Math.pow(angularFreqNatural, 2) - Math.pow(gamma / 2, 2));
			
			return;
		}
		
		public function onComplete(handler:Function):void
		{
			completeHandler = handler;
		}
		
		public function update():void
		{
			var damp:Number = Math.exp( -gamma * time / 2);
			
			if (damp < .01) 
			{
				obj[property] = startValue;
				Game.game.oscilators.removeAt(Game.game.oscilators.indexOf(this));
				isActive = false;
				
				if (completeHandler != null)
				{
					completeHandler(completeObject);
				}
				
				return;
			}
			
			val = startValue + amplitude * damp * Math.sin(angularFreq * time);
			time += timeStep;
			
			obj[property] = val;
		}
	}
}