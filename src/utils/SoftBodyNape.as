/**
 * KerchaGames 2013
 * Sofbody example using Nape,Starling and vertex shader.
 * Custom Starling DisplayObject.
 */

package utils 
{
	import com.adobe.utils.AGALMiniAssembler;
	import nape.phys.Body;

	import flash.display3D.*;
	import flash.geom.*;

	import nape.geom.Vec2;

	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.errors.MissingContextError;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.utils.VertexData;

	public class SoftBodyNape extends DisplayObject
	{
		private static var PROGRAM_NAME:String = "softbody";
		private var mSyncRequired:Boolean;
		
		// custom members
		private var mSegments:uint;
		private var mColor:uint;
		private var mTinted:Boolean;
		
		// vertex data 
		private var mVertexData:VertexData;
		private var mVertexBuffer:VertexBuffer3D;
		private var mExtraVertexBuffer:VertexBuffer3D;
		private var mExtraData:Vector.<Number>;
		
		// index data
		private var mIndexData:Vector.<uint>;
		private var mIndexBuffer:IndexBuffer3D;
		private var mMarkersData:Vector.<Vec2>;
		private var indexMarkerData:Vector.<uint>;
		private var mTexture:Texture;
		
		// help objects (to avoid temporary objects)
		private static var sHelperMatrix:Matrix = new Matrix();
		private static var sRenderAlpha:Vector.<Number> = new <Number>[1.0, 1.0, 1.0, 1.0];
		
		//logical nape model
		public var mSegmentObj:SegmentObjectNape;
		
		public var active:Boolean = false;
		
		private var boundsRect:Rectangle = new Rectangle();
		private var posOffset:Vector.<Number> = new Vector.<Number>();
		
		public function SoftBodyNape ( segmentObj:SegmentObjectNape, indexData:Vector.<uint>, texture:Texture = null, color:uint = 0x5852C7, tinted:Boolean = false )
		{
			this.mSegmentObj = segmentObj;
			this.mTexture = texture;
			this.mTinted = tinted;
			
			this.mMarkersData = segmentObj.getGlobalVertices ();
			
			this.mIndexData = indexData;
			
			mSyncRequired = false;
			
			mSegments = int(mIndexData.length / 3);
			mColor = color;
			
			// setup vertex data and prepare shaders
			setupVertices();
			createBuffers();
			registerPrograms();
			
			// handle lost context
			Starling.current.addEventListener(Event.CONTEXT3D_CREATE, onContextCreated);	
			
			mSegmentObj.segments[0].body.userData.rope = this;
		}
		
		private function onContextCreated(event:Event):void
		{
			// the old context was lost, so we create new buffers and shaders.
			createBuffers();
			registerPrograms();
		}
		
		/**
		 * Update nape vertices
		 */
		public function update():void 
		{
			//no need to update mMarkersData because array is a reference
			mSegmentObj.getGlobalVertices();
		}
		
		public function activate(body1:Body = null, pos:Vec2 = null, mishit:Boolean = false, isTentacle:Boolean = false):void 
		{
			active = true;
			mSegmentObj.activate(body1, pos, mishit, isTentacle);
			Game.game.layer3.addChild(this);
		}
		
		public function deactivate(body1:Body = null):void 
		{
			active = false;
			mSegmentObj.stopActuate();
			mSegmentObj.deactivate(body1);
		}
		
		/** Returns a rectangle that completely encloses the object as it appears in another
		 * coordinate system. */
		
		public override function getBounds(targetSpace:DisplayObject, resultRect:Rectangle = null):Rectangle
		{
			return boundsRect;
			
			/*if (resultRect == null)
				resultRect = new Rectangle();
			var transformationMatrix:Matrix = getTransformationMatrix(targetSpace);
			return mVertexData.getBounds(transformationMatrix, 0, -1, resultRect);*/
		}		
		
		/** Creates the required vertex- and index data and uploads it to the GPU. */
		private function setupVertices():void
		{
			mVertexData = new VertexData(mMarkersData.length);
			mVertexData.setUniformColor(mColor);
			
			mExtraData = new Vector.<Number>();
			
			var marker:Vec2;
			var b:Rectangle = mSegmentObj.bounds;
			
			//one VertexConst can recive postion for two vertices
			for (var i:int = 0; i < Math.ceil(mMarkersData.length / 2); i++)		
			{
				var pair1:int = i * 2; 		//first vertex in pair
				var pair2:int = i * 2 + 1;  //second vertex in pair
				
				var u:Number;
				var v:Number;
				
				if (pair1 < mMarkersData.length)
				{	
					marker = mMarkersData[pair1];
					//UV
					u = (marker.x - b.x) / (b.width);
					v = (marker.y - b.y) / b.height;				
					mVertexData.setPosition(pair1, marker.x, marker.y);
					mVertexData.setTexCoords (pair1, u, v );										
					
					mExtraData.push(5 + i, 1, 0);
				}
				if (pair2 < mMarkersData.length)
				{
					marker = mMarkersData[pair2];
					//mVertexData.setPosition(pair2, marker.x, marker.y);
					
					u = (marker.x - b.x) / (b.width);
					v = (marker.y - b.y) / b.height;
					mVertexData.setPosition(pair2, marker.x, marker.y);
					mVertexData.setTexCoords (pair2, u, v );
					
					//vertex extra data: id, par
					mExtraData.push(5 + i, 0, 1);				
				}			
			}
			
			if (mTexture)
			{
				mVertexData.setPremultipliedAlpha(mTexture.premultipliedAlpha);
				mTexture.adjustVertexData(mVertexData, 0, mVertexData.numVertices);
			}
		}
		
		/** Creates new vertex- and index-buffers and uploads our vertex- and index-data to those
		 *  buffers. */
		private function createBuffers():void
		{
			var context:Context3D = Starling.context;
			if (context == null)
				throw new MissingContextError();
			
			if (mVertexBuffer)
				mVertexBuffer.dispose();
			if (mIndexBuffer)
				mIndexBuffer.dispose();
			
			mVertexBuffer = context.createVertexBuffer(mVertexData.numVertices, VertexData.ELEMENTS_PER_VERTEX);
			mVertexBuffer.uploadFromVector(mVertexData.rawData, 0, mVertexData.numVertices);
			
			mExtraVertexBuffer = context.createVertexBuffer(mVertexData.numVertices, 3);
			mExtraVertexBuffer.uploadFromVector(mExtraData, 0, mVertexData.numVertices);
			
			mIndexBuffer = context.createIndexBuffer(mIndexData.length);
			mIndexBuffer.uploadFromVector(mIndexData, 0, mIndexData.length);
		}
		
		/** Renders the object with the help of a 'support' object and with the accumulated alpha
		 * of its parent object. */
		public override function render(support:RenderSupport, alpha:Number):void
		{
			// always call this method when you write custom rendering code!
			// it causes all previously batched quads/images to render.
			support.finishQuadBatch ();
			//support.raiseDrawCount ();
			//if (mSyncRequired) syncBuffers();
			
			sRenderAlpha[0] = sRenderAlpha[1] = sRenderAlpha[2] = 1.0;
			sRenderAlpha[3] = alpha * this.alpha;
			
			var context:Context3D = Starling.context;
			if (context == null)
				throw new MissingContextError();
			
			// apply the current blendmode
			support.applyBlendMode(false);
			
			// activate program (shader) and set the required buffers / constants 
			context.setProgram(Starling.current.getProgram(PROGRAM_NAME));
			if (mTexture) context.setTextureAt(0, mTexture.base);
			context.setVertexBufferAt(0, mExtraVertexBuffer, VertexData.POSITION_OFFSET, Context3DVertexBufferFormat.FLOAT_3); //instead of position - vertex ID and pair
			context.setVertexBufferAt(1, mVertexBuffer, VertexData.COLOR_OFFSET, Context3DVertexBufferFormat.FLOAT_4);
			context.setVertexBufferAt(2, mVertexBuffer, VertexData.TEXCOORD_OFFSET, Context3DVertexBufferFormat.FLOAT_2);
		
			
			context.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, support.mvpMatrix3D, true);
			context.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 4, sRenderAlpha, 1);
			
			for (var i:int = 0; i < Math.ceil(mMarkersData.length / 2); i++)
			{
				posOffset.splice(0, posOffset.length);
				
				var par1:int = i * 2;
				var par2:int = i * 2 + 1;
				
				var ang:Number = Mut.getAngP(mMarkersData[par1].x, mMarkersData[par1].y, mMarkersData[par2].x, mMarkersData[par2].y);
				
				if ( par1 < mMarkersData.length )
				{
					//mVertexData.setPosition (par1, mMarkersData[par1].x, mMarkersData[par1].y);
					posOffset.push( mMarkersData[par1].x - 3 * Math.cos(ang), mMarkersData[par1].y - 3 * Math.sin(ang));
				}
				if ( par2 < mMarkersData.length )
				{
					//mVertexData.setPosition( par2, mMarkersData[par2].x, mMarkersData[par2].y )
					posOffset.push ( mMarkersData[par2].x + 3 * Math.cos(ang), mMarkersData[par2].y + 3 * Math.sin(ang));
				}
				else
				{
					posOffset.push(1, 1);
				}					
				context.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 5 + i, posOffset, 1); //vertices positions, register == vertex id, x,y == first vertex postion in pair, z,w == second vertex postion in pair
			}
			
		
			// finally: draw the object!
			context.drawTriangles(mIndexBuffer, 0, mSegments);
			
			// reset buffers
			if (mTexture)
				context.setTextureAt(0, null);
			context.setVertexBufferAt(0, null);
			context.setVertexBufferAt(1, null);
			context.setVertexBufferAt(2, null);
			context.setVertexBufferAt(3, null);
		}
		
		private function registerPrograms():void
		{
			var target:Starling = Starling.current;
			if (target.hasProgram(PROGRAM_NAME)) return; // already registered
			
			var vertexProgramCode:String = 
				//"va0" - data for vertex: x = id, y = first in pair, z = second in par
				
				"mov vt0, vc[va0.x] \n" + 				//vertices postition to temp	
				"mov vt1, va0 \n" + 					//temp
				
				"mul vt0.xy, vt0.xyzw, va0.y \n" + 		//clear position
				"mul vt0.zw, vt0.xyzw, va0.z \n" +		//clear position
				
				"add vt1.xy, vt0.xy, vt0.zw \n"+		//add new position	
				
				"m44 op, vt1, vc0 \n" + 				// 4x4 matrix transform to output space*					
				
				"mul v0, va1, vc4 \n" +					 // multiply color with alpha and pass it to fragment shader
				"mov v1, va2  \n" 						// pass texture coordinates to fragment program
				
			
			var tintAlpha:String = mTinted ? "mul ft0, ft0, v0 \n" : "mul ft0.w, ft0.w, v0.w \n";
			var fragmentProgramCode:String = mTexture ?
				"tex ft0, v1, fs0 <2d,clamp,linear> \n" + 
				tintAlpha + 
				"mov oc, ft0 \n" // multiply color with texel color  
				: "mov oc, v0 \n";
			
			var vertexProgramAssembler:AGALMiniAssembler = new AGALMiniAssembler();
			vertexProgramAssembler.assemble(Context3DProgramType.VERTEX, vertexProgramCode);
			
			var fragmentProgramAssembler:AGALMiniAssembler = new AGALMiniAssembler();
			fragmentProgramAssembler.assemble(Context3DProgramType.FRAGMENT, fragmentProgramCode);
			
			target.registerProgram(PROGRAM_NAME, vertexProgramAssembler.agalcode, fragmentProgramAssembler.agalcode);
		}
		
		/** Uploads the raw data of all batched quads to the vertex buffer. */
        private function syncBuffers():void
        {
            if (mVertexBuffer == null) createBuffers();
            else
            {  
                mVertexBuffer.uploadFromVector(mVertexData.rawData, 0, mVertexData.numVertices);
                mSyncRequired = false;
            }
        }
		
		public function get color():uint
		{
			return mColor;
		}
		
		public function set color(value:uint):void
		{
			mColor = value;
			mVertexData.setUniformColor(mColor);
			mSyncRequired = true;
		}
		
		/** Disposes all resources of the display object. */
		public override function dispose():void
		{
			Starling.current.removeEventListener(Event.CONTEXT3D_CREATE, onContextCreated);
			
			if (mVertexBuffer) mVertexBuffer.dispose();
			if (mIndexBuffer) mIndexBuffer.dispose();
			
			mSegmentObj.dispose();
			
			mSegmentObj = null;
			super.dispose();
		}
	}
}