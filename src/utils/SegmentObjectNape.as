/**
 * KerchaGames 2013
 * Sofbody example using Nape,Starling and vertex shader.
 * Nape body.
 */
package utils
{
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;
	import nape.phys.BodyList;
	import starling.display.MovieClip;
	
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Cubic;
	
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.constraint.AngleJoint;
	import nape.constraint.Constraint;
	import nape.constraint.PivotJoint;
	import nape.constraint.WeldJoint;
	import nape.geom.Vec2;
	import nape.space.Space;

	public class SegmentObjectNape
	{
		/**
		 * Segments
		 */
		public var segments:Vector.<Segment>;
		
		/**
		 * Vertices
		 */
		public var vertices:Vector.<Vec2>;
		
		/**
		 * World
		 */
		public var space:Space;
		
		/**
		 * Scale
		 */
		public var scale:Number;
		
		/**
		 * Body dimensions
		 */
		public var bounds:Rectangle;
		
		/**
		 * Median
		 */
		protected var median:uint;
		protected var medianSegment:Segment;

		protected var joints:Vector.<Constraint>;
		
		private var topJoint:PivotJoint;
		
		public var isAdherent:Boolean = false;

		public function SegmentObjectNape( space:Space, scale:Number, bounds:Rectangle, numSegments:int, zeroMass:Boolean = false )
		{
			joints = new Vector.<Constraint>();
			
			this.space = space;
			this.scale = scale;
			this.bounds = bounds;
			
			segments = new Vector.<Segment>();
			
			vertices = new Vector.<Vec2>( numSegments * 2 + 2, true );
			
			for (var i:uint = 0; i < vertices.length; i++) vertices[i] = new Vec2();
			
			
			
			create( bounds, numSegments, zeroMass );
			
			topJoint = new PivotJoint(null, null, new Vec2(), new Vec2(segments[0].body.bounds.width / 2, segments[0].body.bounds.height / 2));
			topJoint.userData.noPush = true;
			joints.push(topJoint);
		}
		
		protected function create( bounds:Rectangle, numSegments:uint, zeroMass:Boolean = false ):void
		{
			var segment:Segment;
			var pSegment:Segment;
			
			var segmentWidth:Number = bounds.width / numSegments;
			var segmentHeight:Number = bounds.height;
			var segmentX:Number = bounds.x;
			var segmentY:Number = bounds.y;
			
			median = Math.floor( numSegments / 2 );
			
			var k:int = -median;
			
			var i:int = -1;
			while ( ++i < numSegments )
			{
				segment = new Segment( space, new Rectangle(( segmentX + segmentWidth * i ) / scale, segmentY / scale, segmentWidth / scale, segmentHeight / scale ) );
				
				segment.body.userData.clingOffset = Vec2.get(segment.body.bounds.width / 2, segment.body.bounds.height / 2);
				
				//if (zeroMass) segment.body.gravMass = 0;
				
				segments.push( segment );
				
				if ( pSegment != null )
				{
					var rotDestination:Number = Math.cos (i / 2) * i / numSegments;
					
					/*var weldJoint:WeldJoint = new WeldJoint( pSegment.body, segment.body, new Vec2( segmentWidth, segmentHeight / 2 * scale ), new Vec2( 0, segmentHeight / 2 * scale ), rotDestination );
					
					weldJoint.stiff = false;
					weldJoint.frequency = 10;
					weldJoint.damping = 5;
					weldJoint.ignore = true
					
					weldJoint.active = false;						
					//weldJoint.space = space;*/
					
					var angleJoint:AngleJoint = new AngleJoint( pSegment.body, segment.body, -.4, .4, 1.0 );
					angleJoint.ignore = true;
					angleJoint.maxForce = 10;
					//angleJoint.stiff = false;
					//angleJoint.space = space;
					//weldJoint.active = false;
					
					var pivotJoint:PivotJoint = new PivotJoint( pSegment.body, segment.body, new Vec2( segmentWidth, segmentHeight / 2 * scale ), new Vec2( 0, segmentHeight / 2 * scale ) );
					//pivotJoint.frequency = 100;
					pivotJoint.frequency = 100;
					pivotJoint.ignore = true;
					pivotJoint.userData.noPush = true;
					//pivotJoint.stiff = false;
					//pivotJoint.space = space;
					//weldJoint.active = false;
					
					//joints.push(weldJoint, pivotJoint, angleJoint);
					
					joints.push(pivotJoint, angleJoint);
				}
				
				pSegment = segment;
				
				k++;
			}
			
			getGlobalVertices ();
			medianSegment = segments[3];
		}
		
		public function activate(body1:Body = null, pos:Vec2 = null, mishit:Boolean = false, isTentacle:Boolean = false):void
		{
			function activateTopJ():void
			{
				topJoint.anchor1 = topJoint.body1.worldPointToLocal(segments[0].body.position);
				topJoint.space = space;
				
				for (var i:uint = 1; i < segments.length; i++)
				{
					var s:Segment = segments[i];
					s.body.rotation = segments[0].body.rotation;
				}
			}
			
			isAdherent = false;
			segments[0].body.userData.replaced = segments[1].body;
			topJoint.body2 = segments[0].body;
			
			for each(var c:Constraint in joints)
			{
				c.space = space;
			}
			
			var angle:Number = 1.36;
			
			if (!isTentacle) 
			{
				if (pos != null)
				{
					segments[0].body.position.set(pos);
				}
				
				var tempBodyList:BodyList = space.bodiesUnderPoint(segments[0].body.position);
				
				if (tempBodyList.length != 0)
				{
					topJoint.body1 = tempBodyList.at(0);
				}
				else
				{
					topJoint.body1 = Game.game.space.world;
				}
				
				activateTopJ();
				
				for (var i:uint = 0; i < segments.length; i++)
				{
					var b:Body = segments[i].body;
					
					b.rotation = angle + Math.PI;
					b.space = space;
					
					if (i > 0) 
					{
						b.position.setxy(segments[i - 1].body.position.x, segments[i - 1].body.position.y + segments[i - 1].body.bounds.height);
					}
				}
			}
			else
			{
				topJoint.body1 = body1;
				topJoint.space = null;
				
				angle = Mut.getAng(pos, body1.position);
				var distance:Number = Mut.distV(pos, body1.position);
				
				if (mishit)
				{
					while (distance > 180)
					{
						distance /= 1.5;
					}
				}
				
				distance /= segments.length;
				
				for (i = 0; i < segments.length; i++)
				{
					b = segments[i].body;
					
					b.rotation = angle + Math.PI;
					b.space = space;
					
					b.allowRotation = false;
					b.velocity.setxy(0, 0);
					b.angularVel = 0;
					b.position.set(pos);
					
					/*if (mishit) 
					{
						continue;
					}*/
					
					var j:Number = segments.length - i;
					
					if (i == 0)
					{
						Actuate.tween(b.position, .1, {x:pos.x + distance * j * Math.cos(angle), y:pos.y + distance * j * Math.sin(angle)}).ease(Cubic.easeIn).onComplete(function():void
						{
							if (!mishit)
							{
								isAdherent = true;
								
								topJoint.anchor1 = topJoint.body1.worldPointToLocal(segments[0].body.position);
								topJoint.space = space;
								Game.game.tentacleStartEffect(segments[0].body.position);
							}
							else
							{
								setTimeout(function():void
								{
									Game.game.hero.removeCling(true);
									isAdherent = true;
								}, 700);
							}
							for each(var s:Segment in segments)
							{
								b = s.body;
								b.allowRotation = true;
								
								if (!mishit)
								{
									b.shapes.at(0).sensorEnabled = true;
								}
								else
								{
									b.shapes.at(0).sensorEnabled = false;
								}
							}
						});
					}
					else
					{
						Actuate.tween(b.position, .1, {x:pos.x + distance * j * Math.cos(angle), y:pos.y + distance * j * Math.sin(angle)}).ease(Cubic.easeIn);
					}
				}
			}
		}
		
		public function stopActuate():void
		{
			Actuate.stop(topJoint.body2.position);
		}
		
		public function deactivate(body1:Body = null):void
		{
			/*for each(var s:Segment in segments)
			{
				s.body.space = null;
			}*/
			
			for (var i:uint = 0; i < segments.length; i++)
			{
				var s:Segment = segments[i];
				s.body.space = null;
				
				var e:MovieClip = Game.game.tentacleRemoveEffects[i];
				e.x = s.body.position.x;
				e.y = s.body.position.y;
				e.currentFrame = 0;
				//e.rotation = Math.PI * 2 * Math.random();
				e.scale = .5 + Math.random() * .5;
				e.play();
				Game.game.layer3.addChild(e);
			}
			
			for each(var c:Constraint in joints)
			{
				c.space = null;
			}
		}
		
		/**
		 * Return global position for all vertices in right order
		 * @return
		 */
		public function getGlobalVertices():Vector.<Vec2>
		{
			var segment:Segment;
			
			var k:int = 0;
			var i:int = -1;
			
			while ( ++i < segments.length )
			{
				segment = segments[ i ];
				
				if ( i <= median )
				{
					vertices[ k ].set(segment.getGlobalVertex( 3, scale ));
					vertices[ k + 1 ].set(segment.getGlobalVertex( 0, scale ));
					
					if ( i == median )
					{
						medianSegment = segment;
						k += 2;
						
						vertices[ k ].set(segment.getGlobalVertex( 2, scale ));
						vertices[ k + 1 ].set(segment.getGlobalVertex( 1, scale ));
					}
				}
				else
				{
					vertices[ k ].set(segment.getGlobalVertex( 2, scale ));
					vertices[ k + 1 ].set(segment.getGlobalVertex( 1, scale ));
				}
				
				k += 2;
			}
			
			return vertices;
		}
		
		public function dispose():void 
		{
			for each(var s:Segment in segments)
			{
				s.dispose();
			}
			
			segments = null;
			
			for each(var c:Constraint in joints)
			{
				c.space = null;
			}
			
			joints = null
		}
	}
}

import flash.geom.Rectangle;

import nape.constraint.PivotJoint;
import nape.geom.Vec2;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.phys.Material;
import nape.shape.Polygon;
import nape.space.Space;

internal class Segment
{
	public var polygon:Polygon;
	public var body:Body;
	
	private var uVec2:Vec2 = new Vec2();

	public function Segment( world:Space, bounds:Rectangle):void
	{
		create( world, bounds);
	}

	protected function create( space:Space, bounds:Rectangle):void
	{
		polygon = new Polygon( Polygon.rect( 0, 0, bounds.width, bounds.height ) );
		body = new Body( BodyType.DYNAMIC, new Vec2( bounds.x, bounds.y ) );
		body.shapes.add( polygon );
		body.userData.worm = true;		
		polygon.material = new Material ( 0, 100, 10, 1 );
		
		//body.space = space;
		
		body.cbTypes.add(Game.game.CB_ROPE);
		//body.shapes.at(0).sensorEnabled = true;
		body.group = Game.game.heroGroup;
		
		//uVec2.setxy(body.bounds.width / 2, body.bounds.height / 2);
		//body.translateShapes(uVec2);
	}
	
	public function getGlobalVertex( id:int, scale:Number ):Vec2
	{
		uVec2.set(polygon.worldVerts.at( id ));
		uVec2.x *= scale;
		uVec2.y *= scale;
		
		return uVec2
	}

	public function dispose():void 
	{
		body.space = null;
		body.userData.worm = null;
		
		body = null;
		
		uVec2.dispose();
	}
}