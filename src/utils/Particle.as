package utils
{
	import starling.textures.Texture;
	import starling.display.Image;

	public class Particle extends Image
	{
		private var angle:Number;
		private var finishScale:Number;
		private var startScale:Number;
		public var isLive:int;
		private var scaleStep1:Number;
		private var scaleStep:Number;
		private var alphaStep1:Number;
		private var alphaStep:Number;
		private var middleScale:Number;
		private var middleTime:Number;
		private var rotationVel:Number;
		private var gravX:Number;
		private var gravY:Number;
		private var vel:Number;
		private var gravAY:Number;
		private var gravAX:Number;
		private var speedUp:Number;
		private var allowVelNegative:Boolean;
		private var alphaIgnore:Boolean;
		private var flickerMax:Number, flickerMin:Number = 0;
		
		public function Particle (texture:Texture, v:Number, spUp:Number, a:Number, avn:int, l:int, it:int, ss:Number, ms:Number, fs:Number, rv:Number, sa:int, ma:int, fa:int, gx:Number, gy:Number, ga:Number, gax:Number, flickerMax:int, flickerMin:int)
		{
			super(texture);
			
			angle = a;
			speedUp = spUp / 10;
			
			if (avn == 1) allowVelNegative = true
			else allowVelNegative = false;
			
			gravX = gx;
			gravY = gy;
			
			gravAY = ga * 10;
			gravAX = gax * 10;
			
			isLive = l;
			vel = v;
			
			scale = ss / 100;
			finishScale = fs / 100;
			middleScale = ms / 100;
			scaleStep = 0;
			
			
			
			rotationVel = rv;
			
			if (flickerMax > 0)
			{
				this.flickerMax = flickerMax / 100;
				this.flickerMin = flickerMin / 100;
				alphaIgnore = true;
			}
			
			if (it == 0) middleTime = l / 2;
			else if (it < l) middleTime = it
			else middleTime = l / 4;
			
			ma /= 100;
			fa /= 100;
			sa /= 100;
			
			alpha = sa;
			
			ms /= 100;
			ss /= 100;
			fs /= 100;
			
			alphaStep = (ma - sa) / middleTime;
			alphaStep1 = (fa - ma) / (l - middleTime);
			
			scaleStep = (ms - ss) / middleTime;
			scaleStep1 = (fs - ms) / (l - middleTime);
		}
		
		public function live():void
		{
			x += vel * Math.cos(angle) + gravX;
			y += vel * Math.sin(angle) + gravY;
			
			if (speedUp != 0) 
			{
				vel += speedUp;
				if (!allowVelNegative && vel < 0) 
				{
					vel = 0;
					speedUp = 0;
				}
			}
			
			if (flickerMax > 0)
			{
				if ( Math.random() > .4) alpha = flickerMin;
				else alpha = flickerMax;	
			}
			
			if (middleTime != 0)
			{
				if(alphaStep != 0) alpha += alphaStep;
				if(scaleStep != 0)scale += scaleStep;
				middleTime--;
			}
			else 
			{
				if(alphaStep1 != 0) alpha += alphaStep1;
				if(scaleStep1 != 0)scale += scaleStep1;
			}
			
			if (gravAY != 0)
			{
				gravY += gravAY;
			}
			if (gravAX != 0)
			{
				gravX += gravAX;
			}
			
			if (rotationVel != 0) rotation += rotationVel;
			
			isLive--;
		}
	}
}