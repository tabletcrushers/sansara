package utils
{
	import nape.geom.Vec2;
	public class Mut
	{
		public static function getAng(p1:Vec2, p2:Vec2):Number
		{
			var a:Number = Math.atan2(p2.y - p1.y, p2.x - p1.x);
			return a;
		}
		
		public static function getAngP(x1:Number, y1:Number, x2:Number, y2:Number):Number
		{
			var a:Number = Math.atan2(y2 - y1, x2 - x1);
			return a;
		}
		
		public static function dist(x1:Number, y1:Number, x2: Number, y2:Number):Number
		{
			return Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
		}
		
		public static function distV(v1:Vec2, v2:Vec2):Number
		{
			return Math.sqrt((v1.x - v2.x)*(v1.x - v2.x) + (v1.y - v2.y)*(v1.y - v2.y));
		}
		
		public static function roundTo(value:Number, base:Number): Number
		{
			return Math.floor((value + base / 2) / base) * base;
		}
		
		public static function lerp( start : Number, end : Number, amt : Number ) : Number
		{
			return start + ( end - start ) * amt;
		}
	}
}