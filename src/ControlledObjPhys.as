package 
{
	import nape.geom.Vec2;
	import starling.display.DisplayObject;
	import nape.phys.Body;
	import starling.display.Sprite;
	import utils.Particles;
	public class ControlledObjPhys extends ControlledObj
	{
		protected var body:Body;
		protected var graphic:DisplayObject;
		
		public function ControlledObjPhys(b:Body, g:DisplayObject, ip:Number)
		{
			super(ip);
			
			if(!(g is Particles)) b.userData.graphic = g;
			body = b;
			body.userData.parentObj = this;
			graphic = g;
		}
		
		override protected function init():void
		{
			body.space = Game.game.space;
			Game.game.layer3.addChild(graphic);
			
			additionalInit();
		}
		
		protected function additionalInit():void
		{
			
		}
		
		protected function additionalClear():void
		{
			
		}
		
		protected function clear():void
		{
			Game.game.clearBody(body);
			
			Game.game.controlledObj.removeAt(Game.game.controlledObj.indexOf(this));
			
			additionalClear();
		}
	}
	
}