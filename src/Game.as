package
{
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Cubic;
	import com.eclecticdesignstudio.motion.easing.Elastic;
	import com.eclecticdesignstudio.motion.easing.Linear;
	import com.eclecticdesignstudio.motion.easing.Sine;	
	import flash.media.Sound;
	import nape.constraint.AngleJoint;
	import nape.constraint.ConstraintList;
	import nape.dynamics.Arbiter;
	
	
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.system.System;
	import flash.ui.Keyboard;
	import flash.display.BitmapData;	
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;
	
	import nape.constraint.PivotJoint;
	import nape.phys.BodyList;
	import nape.phys.Material;
	import nape.shape.Shape;
	import nape.callbacks.CbEvent;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.phys.Compound;
	import nape.shape.Edge;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.space.Space;
	import nape.callbacks.CbType;
	import nape.constraint.Constraint;
	//import nape.util.BitmapDebug;
	import nape.constraint.WeldJoint;
	import nape.dynamics.InteractionGroup;
	import nape.geom.Vec2List;
	import nape.shape.ShapeList;
	
	import starling.core.Starling;
	import starling.display.BlendMode;
	import starling.display.DisplayObject;
	import starling.display.MovieClip;
	import starling.events.KeyboardEvent;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import starling.extensions.ParticleSystem;
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureAtlas;
	import starling.filters.BlurFilter;
	
	//import starling.display.Shape;
	
	import utils.Mut;
	import utils.SegmentObjectNape;
	import utils.SoftBodyNape;
	import utils.SoftPolygon;
	import utils.BodyFromGraphic;
	import utils.ParticlesEm;
	import utils.Particle;
	import utils.Sutils;
	import utils.Oscillator;
	import utils.Particles;
	
	//import utils.BodySplitter;

	public class Game extends Sprite
	{
		//var debug:BitmapDebug = new BitmapDebug(8000, 1800, 0, true);
		
		public static var game:Game;
		
		[Embed(source="../assets/atlases/graphics01.xml",mimeType="application/octet-stream")]
		public static const Graphics01Xml:Class;
		[Embed(source="../assets/atlases/graphics01.png")]
		private static const Graphics01Png:Class;
		
		[Embed(source="../assets/atlases/interface.xml",mimeType="application/octet-stream")]
		public static const InterfaceXml:Class;
		[Embed(source="../assets/atlases/interface.png")]
		private static const InterfacePng:Class;
		
		[Embed(source="../assets/atlases/atlas.xml",mimeType="application/octet-stream")]
		public static const permanentAtlasXml:Class;
		[Embed(source="../assets/atlases/atlas.png")]
		private static const permanentAtlasPng:Class;
		private var permanentAtlas:TextureAtlas;
		
		/*[Embed(source="../assets/temp.png")]
		private static const TempPng:Class;*/
		
		private var interfaceAtlas:TextureAtlas;
		private var atlas:TextureAtlas;
		private var bgAtlas:TextureAtlas;
		
		private var numLayers:uint = 5;
		private var layer0:Sprite;
		private var layer1:Sprite;
		private var layer2:Sprite;
		public var layer3:Sprite;
		private var layer4:Sprite;
		
		public var softBodies:Vector.<SoftPolygon> = new Vector.<SoftPolygon>();
		
		private var bulbEmitters:Vector.<Object> = new Vector.<Object>();
		private var bulbs:Vector.<Body> = new Vector.<Body>();
		private var bulbsLifeAfterTouch:uint = 21;
		
		private var spalls:Vector.<Body> = new Vector.<Body>();
		
		
		public var space:Space;
		private var uVec2:Vec2 = new Vec2(0, -200);
		private var uBodyList:BodyList = new BodyList();
		
		private var previousTouch:Vec2 = new Vec2();
		
		public const CB_GROUND:CbType = new CbType();
		public const CB_BULB:CbType = new CbType();
		public const CB_FIREBALL:CbType = new CbType();
		public const CB_HERO:CbType = new CbType();
		public const CB_ROPE:CbType = new CbType();
		public const CB_BONUS:CbType = new CbType();
		public const CB_SQUEEZE:CbType = new CbType();
		public const CB_DANGER:CbType = new CbType();
		public const CB_PRESS:CbType = new CbType();
		public const CB_SPALL:CbType = new CbType();
		public const CB_PERSECUTOR:CbType = new CbType();
		public const CB_FALL:CbType = new CbType();
		public const CB_JUMP:CbType = new CbType();
		public const CB_BOMB:CbType = new CbType();
		public const CB_FATAL:CbType = new CbType();
		public const CB_SPRING:CbType = new CbType();
		
		public var hero:Hero;
		public var heroGroup:InteractionGroup = new InteractionGroup(true);
		
		private const LEFT:int = -1;
		private const RIGHT:uint = 1;
		private const STOP:uint = 0;
		
		private var controllLeft:Boolean;
		private var controllRight:Boolean;
		private var controllUp:Boolean;
		private var controllDown:Boolean;
		
		private var jumpAble:Boolean = true;
		
		private var deltaScroll:Vec2 = Vec2.get();
		private var currentDeltaScrollInd:int = -1;
		private var deltaScrollStartInd:int = -1;
		
		private var scrollMarkers:Vector.<Object> = new Vector.<Object>();
		private var limitScrollBottom:Vector.<Vec2> = new Vector.<Vec2>();
		private var limitScrollTop:Vector.<Vec2> = new Vector.<Vec2>();
		
		private var bottomLim:Number = Infinity;
		private var topLim:Number = -Infinity;
		
		private var ropes: Vector.<SoftBodyNape>;
		private var activeRopes: Vector.<SoftBodyNape>;
		private var ropeMarkers:Vector.<Object>;
		
		
		[Embed(source="../assets/rope0.png")]
		public var ropeTexture0:Class;
		
		public var emitters:Vector.<ParticlesEm> = new Vector.<ParticlesEm>;
		
		private var currentLevel:uint = 1;
		private var savedLevel:uint = 1;
		
		[Embed(source = "../Assets/particles/splash.xml", mimeType = "application/octet-stream")]
		private const TestXml:Class;
		private var uXml:XML = XML(new TestXml());
		[Embed(source = "../Assets/particles/splash.png")]
		private const FogClass:Class;
		
		private var splash:ParticlesEm;
		
		[Embed(source = "../Assets/particles/tentacleR.xml", mimeType = "application/octet-stream")]
		private const tentacleRemoveXml:Class;
		
		public var tentacleStartEmmiter:ParticlesEm;
		
		
		//particles______________________________
		[Embed(source="../assets/particles/fog_50.pex", mimeType="application/octet-stream")]
        public static const ConfigFog_00:Class;
		[Embed(source = "../assets/particles/fog.png")]
        public static const FogTexture:Class;
		public var fog50:Particles;
		
		[Embed(source="../assets/particles/persecutor.pex", mimeType="application/octet-stream")]
        public static const ConfigFPersecutor:Class;
		[Embed(source = "../assets/particles/persecutor.png")]
        public static const PeresecutorTexture:Class;
		
		public var pivotJointsPool:Vector.<PivotJoint> = new Vector.<PivotJoint>();
		private var chainLinksPool:Vector.<Body> = new Vector.<Body>();
		
		public var angleJointsPool:Vector.<AngleJoint> = new Vector.<AngleJoint>();
		
		private var chainMarkers0:Vector.<Object> = new Vector.<Object>();
		
		private var fireBalls:Vector.<Body> = new Vector.<Body>();
		
		
		private var recoveryPoint:Vec2 = Vec2.get();
		private var recoveryMarkers:Vector.<uint> = new Vector.<uint>();
		private const CHECK_RP_DISTANCE:uint = 280;
		
		private var targetVec:Vec2 = Vec2.get();
		private var cameraFollowPointX:Number = -400;
		private var cameraFollowAcceleration:Number = 1;
		
		private var stepY:Number = 0;
		private var stepX:Number = 0;
		
		private var zoomMarkers:Vector.<Object> = new Vector.<Object>();
		private var currentZoom:Number = 1;
		
		private var blackSquare:Image;
		
		private var realGameStarted:Boolean;
		
		public var pause:Boolean = false;
		private var pauseB:Image;
		
		
		private var zoomOscillator:Oscillator = new Oscillator();		
		private var dtYOscillator:Oscillator = new Oscillator();
		private var dtXOscillator:Oscillator = new Oscillator();
		
		private var cameraDeltaX:Number = 0;
		private var cameraDeltaY:Number = 0;
		
		
		public var controlledObj:Vector.<ControlledObj> = new Vector.<ControlledObj>();
		
		private var rotatingBodies:Vector.<Body> = new Vector.<Body>();
		private var springingBodies:Vector.<Body> = new Vector.<Body>();
		
		public var borderRight:Number;
		public var borderLeft:Number;
		public var borderBottom:Number;
		public var center:Number;
		
		
		private var allBodies:Vector.<Body> = new Vector.<Body>();
		private var graphics:Vector.<DisplayObject> = new Vector.<DisplayObject>();
		
		private var fallGroup:InteractionGroup = new InteractionGroup(true);
		
		private var zoomScaleOld:Number = 1;
		
		private var noInterationGroup:InteractionGroup = new InteractionGroup(true);
		
		private var tmpBody:Body;
		
		private var tShape:starling.display.Shape;
		
		private var l3m:Matrix;
		private var l2m:Matrix;
		private var l1m:Matrix;
		
		private var bodyToClear:BodyList = new BodyList();
		
		public var tentacleRemoveEffects:Vector.<MovieClip>;
		//new Image(atlas.getTexture("armor"));;
		
		//private var testImg:Image;
		
		private var s_fireBall:Sound = new Sound_fireBall();
		
		
		private var frameRate:Number = Starling.current.nativeStage.frameRate;
		
		private const utilCounterLim:uint = uint(frameRate / 3);
		private var utilCounter:uint = utilCounterLim;
		
		private var clearMarkers:Vector.<uint> = new Vector.<uint>();
		
		private var nextToClearCheck:uint = 0;
		
		private const CLEARANCE_L:uint = 420;
		private const CLEARANCE_R:uint = 140;
		private const CLEARANCE_B:uint = 280;
		
		private const ROPE_LONG:uint = 0;
		
		public var oscilators:Vector.<Oscillator> = new Vector.<Oscillator>();
		
		public var delayVectorOff:Vector.<Delay> = new Vector.<Delay>();
		public var delayVectorOn:Vector.<Delay> = new Vector.<Delay>();
		
		
		public function clearGraphic(graphic:DisplayObject):void
		{
			if (graphic != null && graphic.parent != null)
			{
				if (graphic is Sprite)
				{
					while (Sprite(graphic).numChildren > 0)
					{
						Sprite(graphic).getChildAt(0).removeFromParent(true);
					}
				}
				
				graphic.removeFromParent(true);
			}
		}
		
		public function clearBody(b:Body, recursivelyRemove:Boolean = true):void
		{
			if (nextToClearCheck > 0)
			{
				nextToClearCheck--;
			}
			
			b.space = null;
			
			if (b.userData.rope != null)
			{
				ropes.push(b.userData.rope);
				activeRopes.removeAt(activeRopes.indexOf(b.userData.rope));
			}
			
			if (b.userData.ch0) 
			{
				chainLinksPool.push(b);
			}
			
			clearGraphic(b.userData.graphic);
			
			var cList:ConstraintList = b.constraints;
			
			if (cList.empty())
			{
				return;
			}
			
			while (!cList.empty())
			{
				var c:Constraint = cList.pop();
				c.space = null;
				if (c.userData.pool)
				{
					pivotJointsPool.push(c);
				}
			}
		}
		
		
		private function save():void
		{
			Main.main.sharedObj.data.currentLevel = currentLevel;
			Main.main.sharedObj.data.recoveryPoint = recoveryPoint;
			Main.main.sharedObj.flush();
		}
		
		private function removeTentacleEffect(e:Event):void
		{
			MovieClip(e.currentTarget).removeFromParent();
		}
		
		public function Game()
		{
			super();
			
			for (var i:uint = 0; i < 2; i++)
			{
				delayVectorOff.push(new Delay());
			}
			
			
			zoomOscillator.setup(null, null, 1 / 10, 100, 7, 2, .5);
			dtYOscillator.setup(null, null, 1 / 4, 0, 42, 2, .5);
			dtXOscillator.setup(null, null, 1 / 2, 0, 21, 2, .5);
			
			if (Main.main.sharedObj.size > 0) 
			{ 
				recoveryPoint.setxy(Main.main.sharedObj.data.recoveryPoint.x, Main.main.sharedObj.data.recoveryPoint.y);
				currentLevel = Main.main.sharedObj.data.currentLevel;
			}
			
			var texture:Texture = Texture.fromBitmap(new InterfacePng());
			var xml:XML = XML(new InterfaceXml());
			interfaceAtlas = new TextureAtlas(texture, xml);
			
			texture = Texture.fromBitmap(new permanentAtlasPng());
			xml = XML(new permanentAtlasXml());
			permanentAtlas = new TextureAtlas(texture, xml);
			
			tentacleRemoveEffects = new Vector.<MovieClip>;
			
			for (i = 0; i < 14; i++)
			{
				var c:MovieClip = new MovieClip(permanentAtlas.getTextures("tentacleR"), 21 + uint(35 * Math.random()));
				tentacleRemoveEffects.push(c);
				c.blendMode = BlendMode.ADD;
				Starling.juggler.add(c);
				c.addEventListener(Event.COMPLETE, removeTentacleEffect);
				c.pivotX += c.width / 2;
				c.pivotY += c.height / 2;
				c.rotation = Math.PI * 2 * Math.random();
				c.loop = false;
			}
			
			game = this;
			
			this.scaleX /= Main.main.scaleFactor;
			this.scaleY /= Main.main.scaleFactor;
			
			//Main.main.debug.scaleX /= Main.main.scaleFactor; Main.main.debug.scaleY /= Main.main.scaleFactor; debug.drawConstraints = true; debug.drawSensorArbiters = true;
			
			for (i = 0; i < numLayers; i++) 
			{
				this["layer" + i] = new Sprite();
				addChild(this["layer" + i]);
			}
			
			l3m = layer3.transformationMatrix;
			l2m = layer2.transformationMatrix;
			l1m = layer1.transformationMatrix;
			
			//testImg = new Image(interfaceAtlas.getTexture("pause"));
			//layer3.addChild(testImg);
			//testImg.pivotX = testImg.pivotY = 21;
			
			splash = new ParticlesEm(permanentAtlas.getTexture("trp"), uXml, layer3, 70);
			emitters.push(splash);
			
			uXml = XML(new tentacleRemoveXml());
			tentacleStartEmmiter = new ParticlesEm(permanentAtlas.getTexture("trp"), uXml, layer3, 140);
			emitters.push(tentacleStartEmmiter);
			
			space = new Space(new Vec2(0, 1000));
			space.worldLinearDrag = 0.28;
			space.worldAngularDrag = 0.28;
			
			addEventListener(Event.ENTER_FRAME, enterFrame);
			
			addEventListener(TouchEvent.TOUCH, onTouch);
			addEventListener(KeyboardEvent.KEY_UP, onKu);
			addEventListener(KeyboardEvent.KEY_DOWN, onKd);
			
			//startGame();
			
			blackSquare = new Image(interfaceAtlas.getTexture("black"));
			blackSquare.width = 2800;
			blackSquare.height = 1400;
			blackSquare.y -= 560;
			blackSquare.x -= 420;
			addChild(blackSquare);
			
			pauseB = new Image(interfaceAtlas.getTexture("pause"));
			pauseB.pivotX = pauseB.pivotY = 21;
			pauseB.x = Main.main.gameWidth - 28;
			pauseB.y = 28;
		}
		
		private function makeBody(x:Number, y:Number, shape:Shape, rotation:Number = 0):Body
		{
			var tb:Body = new Body();
			tb.position.x = x;
			tb.position.y = y;
			tb.rotation = rotation;
			tb.shapes.add(shape);
			//tb.space = space;
			
			return tb;
		}
		
		//var tempBody4watch:Body;
		public function startGame(curLevel:uint):void
		{
			if (curLevel < currentLevel)
			{
				currentLevel = curLevel;
			}
			
			currentLevel = curLevel;
			currentDeltaScrollInd = deltaScrollStartInd;
			
			Actuate.stop(l3m);
			
			for each(var mc:MovieClip in tentacleRemoveEffects)
			{
				Starling.juggler.add(mc);
			}
			
			if (blackSquare.parent == null) 
			{
				addChild(blackSquare);
			}
			
			blackSquare.alpha = 1;
			l3m.a = l3m.d = .7;
			addChild(pauseB);
			
			//currentZoom = 4;
			Actuate.tween(l3m, 4, { a:currentZoom, d:currentZoom } ).ease(Cubic.easeOut).onComplete(function():void
			{
				
			});
			
			Actuate.tween(blackSquare, 1.4, { alpha:0 } ).ease(Linear.easeNone).onComplete(function():void
			{
				removeChild(blackSquare);
				realGameStarted = true;
			});
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CbType.ANY_BODY, CB_SPRING, body_spring));
			//space.listeners.add(new InteractionListener(CbEvent.END, InteractionType.SENSOR, CbType.ANY_BODY, CB_SPRING, body_springE));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_HERO, CB_FATAL, hero_fatal));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.SENSOR, CB_HERO, CB_BOMB, hero_bomb));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_HERO, CB_JUMP, hero_jump));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_HERO, CB_BULB, hero_bulb));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_HERO, CB_FIREBALL, hero_fireball));
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_GROUND, CB_FIREBALL, ground_fireball));
			
			space.listeners.add(new InteractionListener(CbEvent.END, InteractionType.COLLISION, CB_HERO, CB_GROUND, hero_groundE));
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.COLLISION, CB_HERO, CB_GROUND, hero_groundO));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.SENSOR, CB_HERO, CB_BONUS, hero_bonus));
			
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.SENSOR, CB_HERO, CB_ROPE, hero_rope));
			space.listeners.add(new InteractionListener(CbEvent.END, InteractionType.SENSOR, CB_HERO, CB_ROPE, hero_ropeE));
			
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.SENSOR, CB_HERO, CB_SQUEEZE, hero_sq));
			space.listeners.add(new InteractionListener(CbEvent.END, InteractionType.SENSOR, CB_HERO, CB_SQUEEZE, hero_sqE));
			
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.SENSOR, CB_HERO, CB_DANGER, hero_danger));
			space.listeners.add(new InteractionListener(CbEvent.END, InteractionType.SENSOR, CB_HERO, CB_DANGER, hero_dangerE));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_PRESS, CB_GROUND, press_ground));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_HERO, CB_SPALL, hero_spall));
			
			space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, CB_HERO, CB_FALL, hero_fall));
			
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.COLLISION, CB_HERO, CB_PRESS, hero_press));
			
			space.listeners.add(new InteractionListener(CbEvent.ONGOING, InteractionType.SENSOR, CB_HERO, CB_PERSECUTOR, hero_persecutor));
			
			
			//var chainTexture:Texture = atlas.getTexture("chainLink");
			function makeChainLinks(amt:uint, texture:Texture):void
			{
				for (i = 0; i < amt; i++)
				{
					//var tb:Body = makeBody(0, 0, new Circle(15));
					
					var tb:Body = new Body();
					tb.shapes.add(new Circle(17));
					tb.shapes.at(0).sensorEnabled = true;
					tb.shapes.at(0).filter.sensorGroup = 2;
					tb.cbTypes.add(CB_ROPE);
					
					img=new Image(texture)
					img.pivotX = img.width / 2;
					img.pivotY = img.height / 2;
					
					tb.userData.graphic = img;
					
					tb.userData.clingAble = true;
					
					tb.userData.ch0 = true;
					
					chainLinksPool.push(tb);
				}
			}
			
			function makePivotJoints(amt:uint):void
			{
				for (i = 0; i < amt; i++)
				{
					var pivotJoint:PivotJoint = new PivotJoint(new Body(), new Body(), new Vec2(0, 0), new Vec2(0, 30));
					pivotJoint.ignore = true;
					pivotJoint.frequency = 100;
					pivotJoint.ignore = true;
					pivotJoint.userData.pool = true;
					pivotJointsPool.push(pivotJoint);
				}
			}
			
			function makeRopes(amt:uint):void
			{
				for (var i:uint = 0; i < amt; i++)
				{
					makeRope(0, 0, 350, 7);
				}
			}
			
			function makeRope(x:Number, y:Number, w:Number, h:Number):void
			{
				if (ropes == null) 
				{
					ropes = new Vector.<SoftBodyNape> ();
				}
				
				var numSegments:uint = uint(w / 15);
				var segmentObject:SegmentObjectNape = new SegmentObjectNape 
				(
					space,
					Starling.contentScaleFactor,
					new Rectangle (x, y, w, h),
					numSegments
				);
				
				var indexData:Vector.<uint> = new Vector.<uint> ();
				
				for (var i:int = 0; i < segmentObject.segments.length * 2; i++) 
				{
					indexData.push (i , i + 1, i + 2 );
				}
				
				for (i = 0; i < segmentObject.segments.length; i++) 
				{
					segmentObject.segments[i].body.userData.clingAble = true;
				}
				
				var rope:SoftBodyNape = new SoftBodyNape ( segmentObject, indexData, Texture.fromBitmap(new ropeTexture0(), false), 0xFFFFFF*Math.random(), false );
				//layer3.addChild (rope);
				
				ropes.push(rope);
			}
			
			function makeImg(mc:flash.display.MovieClip, x:Number, y:Number, target:Sprite = null, rotation:Number = 0, scale:Number = 1):Image
			{
				var img:Image = Sutils.makeImg(mc);
				img.y = item.y;
				img.x = x;
				img.rotation = rotation;
				img.scale = scale;
				
				if (target != null) 
				{
					target.addChild(img);
				}
				
				return img;
			}
			
			function imgFromAtlas(name:String, item:flash.display.MovieClip):Image
			{
				img = new Image(atlas.getTexture("bomb"));
				img.pivotX = img.width / 2;
				img.pivotY = img.height / 2;
				img.x = item.x + w;
				img.y = item.y;
				
				return img;
			}
			
			function bodyFromMc(item:flash.display.MovieClip):Body
			{
				var r:Number = item.rotation;
				item.rotation = 0;
				var b:Body = BodyFromGraphic.bodyFromDisplayObj(item);
				b.position.setxy(item.x + w - b.userData.graphicOffset.x, item.y - b.userData.graphicOffset.y);
				rotateBody(b, r);
				return b;
			}
			
			function rotateBody(object:Body, angle:Number):void
			{
				uVec2.setxy(object.position.x + object.userData.graphicOffset.x, object.position.y + object.userData.graphicOffset.y);
				var a0:Number = 0;
				var s0:Number = Math.sin(a0);
				var c0:Number = Math.cos(a0);
				var dX0:Number = object.position.x - uVec2.x;
				var dY0:Number = object.position.y - uVec2.y;
				
				object.rotation = 0;
				object.position.x = Math.round(uVec2.x + dX0 * c0 - dY0 * s0);
				object.position.y = Math.round(uVec2.y + dX0 * s0 + dY0 * c0);
				
				// new rotation
				var r:Number = angle * Math.PI / 180;
				var s:Number = Math.sin(r);
				var c:Number = Math.cos(r);
				var dX:Number = object.position.x - uVec2.x;
				var dY:Number = object.position.y - uVec2.y;
				
				object.rotation = r;
				object.position.x = uVec2.x + dX * c - dY * s;
				object.position.y = uVec2.y + dX * s + dY * c;
			}
			
			var j:uint = 0;
			var w:Number = .0;
			var pivotJoints:Vector.<Vec2> = new Vector.<Vec2>;
			var debrises:Object = { };
			
			var heroState:uint = 0;
			
			var longBg:Boolean = false;
			switch(currentLevel)
			{
				case 1: 
					var LibLevel:flash.display.MovieClip = new L001();
					
					atlas = new TextureAtlas(Texture.fromBitmap(new Graphics01Png()), XML(new Graphics01Xml()));
					makeChainLinks(19, atlas.getTexture("chainLink"));
					makePivotJoints(19);
					makeRopes(3);
					
					deltaScroll.x = Main.main.gameWidth * 28 / 100;
					deltaScroll.y = -Main.main.gameHeight * 21 / 100;
					
					if (recoveryPoint.x == 0)
					{ 
						recoveryPoint.setxy(9552, 1187);
						recoveryPoint.setxy(16118, -1700);
						//recoveryPoint.setxy(17044, -1828);
						recoveryPoint.setxy(250, 750);
					}
					
					break;
				case 2: 
					LibLevel = new L002(); 
					heroState = 1;
					recoveryPoint.setxy(280, 700);
					longBg = true;
					break;
			}
			
			hero = new Hero();
			hero.activateSoftBody(recoveryPoint);
			hero.state = heroState;
			
			img = new Image(atlas.getTexture("armor"));
			img.pivotX = img.pivotY = 40;
			hero.setupArmor(img);
			
			var testBody:Body;
			
			for (var part:uint = 0; part < LibLevel.numChildren; part++)
			{
				var level:flash.display.MovieClip = flash.display.MovieClip(LibLevel.getChildByName("part" + part));
				w = j * 2048;
				if (w < 0) w = 0;
				var item:flash.display.MovieClip;
				var img:Image;
				
				for (var i:uint = 0; i <= level.numChildren - 1; i++ )
				{
					item = flash.display.MovieClip(level.getChildAt(i));
					var n:String = item.name;
					
					if (item.x + item.width < recoveryPoint.x - 1400 && n.substr(0, 2) != "bg") 
					{
						continue;
					}
					
					var bd:BitmapData;
					img = null;
					
					if (n.substr(0, 3) == "ch0") 
					{
						var e:Vec2 = null;
						
						if (n.substr(7, 1) != "") 
						{
							e = new Vec2(uint(n.substr(7, 4)) + w, uint(n.substr(12, 4)));
						}
						
						var tc:Object = { pos:new Vec2(item.x + w, item.y), len:uint(n.substr(4, 2)), end:e };
						chainMarkers0.push(tc);
					}
					else if (n.substr(0, 2) == "gr") 
					{
						if (n.substr(3, 1) == "a")
						{
							img = new Image(atlas.getTexture(n.substr(7, 6)));
							img.x = item.x + w;
							img.y = item.y;
						}
						else 
						{
							img = tb.userData.graphic = makeImg(item, item.x + w, item.y);
						}
						
						img.rotation = item.rotation * Math.PI / 180;
						img.scaleX = item.scaleX;
						img.scaleY = item.scaleY;
						layer3.addChild(img);
					}
					else if (n.substr(0, 2) == "rp") 
					{
						recoveryMarkers.push(uint(item.x + w));
					}
					else if (n.substr(0, 2) == "sq") 
					{
						var tb:Body = makeBody(item.x + w, item.y, new Circle(item.width / 2));
						allBodies.push(tb);
						//tb.position.setxy(item.x + w - tb.userData.graphicOffset.x, item.y - tb.userData.graphicOffset.y);
						tb.type = BodyType.STATIC;
						tb.cbTypes.add(CB_SQUEEZE);
						tb.shapes.foreach(function(s:Shape):void
						{
							s.sensorEnabled = true;
						});
					}
					else if (n.substr(0, 2) == "sm") 
					{
						var zx:int = 1;
						var zy:int = 1;
						
						if (n.substr(4, 1) == "m") 
						{
							zx =-1;
						}
						if (n.substr(8, 1) == "m") 
						{
							zy =-1;
						}
						
						scrollMarkers.push( { pos:item.x + w, dt:new Vec2(uint(n.substr(5, 2)) * zx, uint(n.substr(9, 2)) * zy) } );
					}
					else if (n.substr(0, 2) == "bl") 
					{
						limitScrollBottom.push(Vec2.get(item.x + w, item.y));
					}
					else if (n.substr(0, 2) == "tl") 
					{
						limitScrollTop.push(Vec2.get(item.x + w, item.y));
					}
					else if (n.substr(0, 2) == "zf") 
					{
						var  zf:Number = uint(n.substr(3, 4)) / 1000;
						zoomMarkers.push( { pos:uint(item.x + w), val:zf, time:uint(n.substr(8, 1)) } );
					}
					else if (n.substr(0, 2) == "bg") 
					{
						bd = new BitmapData(item.width, item.height, true, 0x0);
						bd.draw(item);
						var txt:Texture = Texture.fromBitmapData(bd, false);
						img = new Image(txt);
						img.x = 0;
						img.y = item.y;
						this["layer" + n.substr(2, 1)].addChild(img);
						
						img = new Image(txt);
						img.x = 2100;
						img.y = item.y;
						this["layer" + n.substr(2, 1)].addChild(img);
						
						if (longBg)
						{
							
							img = new Image(txt);
							img.x = 4200;
							img.y = item.y;
							this["layer" + n.substr(2, 1)].addChild(img);
						}
					}
					else if (n.substr(0, 2) == "gb") 
					{
						tb = bodyFromMc(item);
						
						tb.userData.graphic = makeImg(item, item.x + w, item.y);
						tb.type = BodyType.STATIC;
						tb.cbTypes.add(CB_GROUND);
						tb.group = fallGroup;
						
						allBodies.push(tb);
					}
					else if (n == "clear") 
					{
						clearMarkers.push(uint(item.x + w));
					}
					/*else if (n == "temp") 
					{
						tShape = new starling.display.Shape();
						layer3.addChild(tShape);
						
						tmpBody = BodyFromGraphic.bodyFromDisplayObj(item);
						tmpBody.position.setxy(item.x + w - tmpBody.userData.graphicOffset.x, item.y - tmpBody.userData.graphicOffset.y);
						tmpBody.position.setxy(item.x + w - tmpBody.userData.graphicOffset.x, item.y - tmpBody.userData.graphicOffset.y);
						
						tmpBody.userData.graphicOffset.setxy(-100,-100);
						rotateBody(tb, item.rotation);
						
						var vertices:Vec2List = tmpBody.shapes.at(0).castPolygon.localVerts;
						
						txt = Texture.fromBitmap(new TempPng());
						
						tShape.graphics.lineStyle( -1);
						tShape.graphics.beginTextureFill(txt);
						tShape.graphics.moveTo(vertices.at(0).x + 100, vertices.at(0).y + 100);
						
						for (var k:uint = 1; k < vertices.length; k++)
						{
							var vert:Vec2 = vertices.at(k);
							tShape.graphics.lineTo(vert.x + 100, vert.y + 100);
						}
						
						tShape.graphics.endFill();
						
						tmpBody.userData.graphic = tShape;
						tmpBody.space = space;
					}*/
					else if (n.substr(0, 4) == "bomb")
					{
						tb = new Body(BodyType.STATIC);
						tb.position.setxy(item.x + w, item.y);
						tb.shapes.add(new Circle(uint(n.substr(5, 4))));
						tb.userData.explTime = uint(n.substr(10, 4));
						tb.shapes.at(0).sensorEnabled = true;
						tb.cbTypes.add(CB_BOMB);
						img = new Image(atlas.getTexture("bomb"));
						img.pivotX = img.pivotY = 20;
						img.x = item.x + w;
						img.y = item.y;
						tb.userData.graphic = img;
						allBodies.push(tb);
					}
					else if (n.substr(0, 6) == "jumper")
					{
						var r:Number = item.rotation;
						item.rotation = 0;
						tb = new Body(BodyType.STATIC);
						tb.position.setxy(item.x + w + 40, item.y + 20);
						tb.shapes.add(new Polygon(Polygon.box(80, 40)));
						tb.cbTypes.add(CB_JUMP);
						tb.userData.graphicOffset = Vec2.get( -40, -20);
						rotateBody(tb, r);
						tb.userData.pushForce = uint(n.substr(7, n.length));
						allBodies.push(tb);
					}
					else if (n.substr(0, 2) == "dy") 
					{
						tb = bodyFromMc(item);
						tb.cbTypes.add(CB_GROUND);
						//tb.rotation = rotateBody(tb, r);
						
						var objType:String = n.substr(5, 1);
						if (objType == "a" || objType == "f" || objType == "r" || objType == "b" || objType == "s" || objType == "d" || objType == "c" || objType == "x" || objType == "m")
						{
							if (n.substr(3, 1) == "a")
							{
								img = new Image(atlas.getTexture(n.substr(7, 6)));
							}
							else 
							{
								img = makeImg(item, item.x + w, item.y);
							}
							
							tb.userData.graphic = img;
							
							allBodies.push(tb);
							
							img.x = tb.position.x + tb.userData.graphicOffset.x;
							img.y = tb.position.y + tb.userData.graphicOffset.y;
							
							if (objType == "f")
							{
								tb.cbTypes.add(CB_FALL);
								tb.allowMovement = false;
								tb.allowRotation = false;
								tb.group = fallGroup;
								
								tb.userData.fallTime = uint(n.substr(14, 4));
								
								var linkMarker:flash.display.DisplayObject = level.getChildByName("fpj" + n.substr(19, 2));
								
								if (linkMarker != null)
								{
									uVec2.setxy(linkMarker.x + w, linkMarker.y);
									var joint:PivotJoint = new PivotJoint(tb, space.world, tb.worldPointToLocal(uVec2), uVec2);
									joint.stiff = false;
									tb.userData.link = joint;
								}
							}
							if (objType == "s")
							{
								tb.userData.fallTime = uint(n.substr(14, 4));
								
								//tb.cbTypes.add(CB_SPRING);
								
								linkMarker = level.getChildByName("swj" + n.substr(14, 2));
								
								if (linkMarker != null)
								{
									uVec2.setxy(linkMarker.x + w, linkMarker.y);
									var pj:PivotJoint = new PivotJoint(tb, null, tb.worldPointToLocal(uVec2), uVec2);
									tb.userData.link1 = pj;
									
									var numerator:uint = uint(n.substr(17, 2));
									
									if (numerator != 0)
									{
										var frq:Number = numerator / uint(n.substr(20, 2));
										
										var wj:WeldJoint = new WeldJoint(tb, null, tb.worldPointToLocal(uVec2), uVec2, -tb.rotation);
										wj.stiff = false;
										wj.frequency = frq;
										tb.userData.link0 = wj;
										
										trace(img.pivotX);
										
										if (n.substr(23, 2) != "")
										{
											if (n.substr(23, 2) != "sp")
											{
												tb.gravMass /= uint(n.substr(23, 2));
											}
											else
											{
												if (n.substr(3, 1) == "a")
												{
													img = new Image(atlas.getTexture(n.substr(7, 6)));
												}
												else 
												{
													img = makeImg(item, item.x + w, item.y);
												}
												
												img.pivotX = img.width / 2;
												img.pivotY = img.height;
												tb.userData.graphicOffset1 = Vec2.get(tb.userData.graphicOffset.x + img.pivotX, tb.userData.graphicOffset.y + img.pivotY);
												tb.userData.graphic1 = img;
												tb.cbTypes.add(CB_SPRING);
												tb.userData.oscilator = new Oscillator();
												tb.userData.startRotation = tb.rotation;
												tb.userData.startPosition = tb.position;
												tb.userData.addToRuntime = tb;
												tb.userData.dontMove = true;
												tb.userData.frq = frq;
												tb.gravMass = 0;
												tb.cbTypes.add(CB_SPRING);
												wj.frequency *= 14;
											}
										}
									}
									
									tb.group = noInterationGroup;
								}
							}
							else if (objType == "r")
							{
								tb.allowMovement = false;
								
								if (n.substr(7, 4) == "rotd")
								{
									item = level.getChildByName(n.substr(7, 6)) as flash.display.MovieClip;
									var sensor:Body = BodyFromGraphic.bodyFromDisplayObj(item);
									sensor.position.setxy(item.x + w - tb.userData.graphicOffset.x, item.y - tb.userData.graphicOffset.y);
									sensor.cbTypes.add(CB_DANGER);
									
									sensor.shapes.foreach(function(s:Shape):void
									{
										s.sensorEnabled = true;
									});
									
									tb.userData.sensorBody = sensor;
									tb.userData.dangerOffset = Vec2.get(tb.position.x - sensor.position.x, tb.position.y - sensor.position.y);
								}
								if (n.substr(7, 4) == "rots")
								{
									if (n.substr(14, 1) != "m")
									{
										tb.userData.angVel = uint(n.substr(15, 1)) / uint(n.substr(17, 1));
									}
									else
									{
										tb.userData.angVel = -1 * uint(n.substr(15, 1)) / uint(n.substr(17, 1)); 
									}
									
									tb.userData.rotated = true; 
								}
							}
							else if (objType == "b")
							{
								tb.allowMovement = false;
								tb.allowRotation = false;
								tb.cbTypes.add(CB_FALL);
								tb.userData.fallTime = uint(n.substr(22, 4));
								
								if (n.substr(27, 1) == "a") tb.userData.armored = true;
								
								var key:String = n.substr(14, 7);
								var checkGroup:String = key.substr(0, 2);
								
								if (debrises[key] == undefined)
								{
									if (checkGroup != "no") 
									{
										var g:InteractionGroup = new InteractionGroup(true);
										tb.group = g;
									}
									
									debrises[key] = new Compound();
									tb.compound = debrises[key];
								}
								else
								{
									if (checkGroup != "no") 
									{
										tb.group = debrises[key].bodies.at(0).group;
									}
									
									tb.compound = debrises[key];
								}
							}
							else if (objType == "c")
							{
								tb.allowMovement = false;
								tb.allowRotation = false;
								tb.cbTypes.add(CB_SPALL);
								tb.userData.fallDistance = uint(n.substr(14, 3));
								tb.setShapeMaterials(Material.rubber());
								if (n.substr(18, 1) == "d") tb.userData.dontMove = true;
							}
						}
					}
					else if (n.substr(0, 2) == "bn") 
					{
						tb = BodyFromGraphic.bodyFromDisplayObj(item);
						tb.position.setxy(item.x + w - tb.userData.graphicOffset.x, item.y - tb.userData.graphicOffset.y);
						tb.userData.graphic = makeImg(item, item.x + w, item.y, layer3);
						tb.shapes.at(0).sensorEnabled = true;
						tb.userData.type = item.name.substr(3, 2);
						tb.type = BodyType.STATIC;
						tb.cbTypes.add(CB_BONUS);
						
						allBodies.push(tb);
						
					}
					else if (n.substr(0, 2) == "fb" && n.length == 4) 
					{
						var shape:Shape = new Circle(14);
						tb = makeBody(item.x + w, item.y, shape);
						tb.gravMass /= 2;
						tb.cbTypes.add(CB_FIREBALL);
						//tb.allowMovement = tb.allowRotation = false;
						
						img = new Image(atlas.getTexture("fireball"));
						tb.userData.graphic = img;
						img.pivotX = img.width / 2;
						img.pivotY = img.height / 2;
						
						tb.shapes.at(0).material = Material.rubber();
						
						j = 0;
						var childMc:flash.display.MovieClip = level.getChildByName(n + "_" + j) as flash.display.MovieClip;
						while (childMc != null)
						{
							if (tb.userData.childBalls == null) tb.userData.childBalls = new Vector.<Body>();
							
							var b:Body = tb.copy();
							tb.userData.childBalls.push(b);
							img = new Image(atlas.getTexture("fireball"));
							b.userData.graphic = img;
							img.pivotX = img.width / 2;
							img.pivotY = img.height / 2;
							b.shapes.at(0).material = Material.rubber();
							b.position.setxy(childMc.x + w, childMc.y);
							
							j++;
							childMc = level.getChildByName(n + "_" + j) as flash.display.MovieClip;
						}
						
						fireBalls.push(tb);
					}
					else if (n.substr(0, 2) == "sb")
					{
						tb = makeBody(item.x + w, item.y, new Polygon(Polygon.box(item.width, item.height)), item.rotation);
						tb.type = BodyType.STATIC;
						tb.cbTypes.add(CB_GROUND);
						
						allBodies.push(tb);
					}
					else if (n.substr(0, 5) == "press")
					{
						uVec2.setxy(item.x + w + item.width / 2, item.y + item.height / 2);
						if (n.substr(8, 1) == "a") img = new Image(atlas.getTexture(n.substr(0, 7)))
						else img = makeImg(item, item.x + w, item.y);
						
						tb = makeBody(uVec2.x, uVec2.y, new Polygon(Polygon.box(img.width - 4, img.height - 4)), 0);
						tb.allowRotation = tb.allowMovement = false;
						
						var z:int;
						n.substr(10, 1) == "m"?z =-1:z = 1;
						var deltaOpen:int = uint(n.substr(11, 3)) * z;
						
						n.substr(15, 1) == "m"?z =-1:z = 1;
						var closeVel:int = uint(n.substr(16, 2)) * z;
						
						n.substr(19, 1) == "m"?z =-1:z = 1;
						var openVel:int = uint(n.substr(20, 2)) * z;
						
						var fog:Particles;
						switch(uint(n.substr(23, 2)))
						{
							case 0: fog = new Particles(ConfigFog_00, FogTexture);
						}
						fog.init();
						
						controlledObj.push(new Press(tb, img, uVec2.x, deltaOpen, closeVel, openVel, fog));
					}
					else if (n.substr(0, 5) == "bu")
					{
						bulbEmitters.push( { pos:Vec2.get(item.x + w, item.y), delay:0 } );
					}
					else if (item is Chain)
					{
						img = new Image(atlas.getTexture("chainLink"));
						img.pivotX = img.width / 2;
						img.pivotY = img.height / 2;
						layer3.addChild(img);
						
						tb = makeBody(item.x + w + img.width / 2, item.y + img.height / 2, new Circle(15));
						tb.shapes.at(0).sensorEnabled = true;
						tb.shapes.at(0).filter.sensorGroup = 2;
						tb.cbTypes.add(CB_ROPE);
						tb.userData.graphic = img;
					}
					else if (item is Marker)
					{
						if (n == "pj") 
						{
							pivotJoints.push(new Vec2(item.x + w, item.y));
						}
						else if (n == "velcro") 
						{
							img = imgFromAtlas("bomb", item);
							tb = makeBody(item.x + w, item.y, new Circle(20));
							tb.type = BodyType.STATIC;
							tb.cbTypes.add(CB_GROUND);
							tb.userData.graphic = img;
							hero.velcros.push(tb);
							allBodies.push(tb);
						}
						else if (n.substr(0, 4) == "rope") 
						{
							//var length:uint = uint(n.substr(4, 3));
							//makeRope(item.x + w, item.y, length, 7);
							
							if (ropeMarkers == null)
							{
								ropeMarkers = new Vector.<Object>();
								activeRopes = new Vector.<SoftBodyNape>();
							}
							
							ropeMarkers.push({ pos:Vec2.get(item.x + w, item.y), type:0 });
						}
						else if (n.substr(0, 4) == "purs")
						{
							tb = makeBody(item.x + w, item.y, new Circle(40));
							
							img = new Image(atlas.getTexture("bomb"));
							img.pivotX = img.width / 2;
							img.pivotY = img.height / 2;
							img.x = item.x + w;
							img.y = item.y;
							
							uVec2.setxy(item.x + w, item.y);
							tb.userData.graphic = img;
							
							var pursuer:Pursuer = new Pursuer(tb, img, uVec2, uint(n.substr(5, 4)), uint(n.substr(10, 4)));
							
							controlledObj.push(pursuer);
						}
						else if (n.substr(0, 10) == "persecutor") 
						{
							tb = makeBody(item.x + w, item.y, new Polygon(Polygon.box(210, 400)));
							
							var prt:Particles = new Particles(ConfigFPersecutor, PeresecutorTexture);
							prt.init();
							layer3.addChild(prt);
							//prt.startPS(400, 700);
							
							img = new Image(atlas.getTexture("fly"));
							img.pivotX = img.width / 2;
							img.pivotY = img.height / 2;
							
							var tp:Persecutor = new Persecutor(tb, tb.position.x + 280, 280, new VibroGraphic(img));
							controlledObj.push(tp);
							
							var id:uint = uint(n.substr(11, 1));
							
							var num:uint = 0;
							var pathItem:flash.display.MovieClip;
							
							//tp.pathMarkers.push(new Vec2(item.x + w, item.y));
							
							do
							{
								pathItem = flash.display.MovieClip(level.getChildByName("p" + id + "_" + num));
								if (pathItem == null) break;
								tp.pathMarkers.push(Vec2.get(pathItem.x + w, pathItem.y));
								num++;
							}
							while (true)
						}
					}
				}
				j++;
			}
			
			//constraint
			
			/*
			for (i = 0; i < pivotJoints.length; i++)
			{
				var bs:BodyList = space.bodiesInCircle(pivotJoints[i], 4);
				
				if (bs.length == 0) continue;
				
				var b1:Body = bs.at(0);
				var b2:Body;
				
				if (bs.length < 2) b2 = space.world;
				else b2 = bs.at(1);
				
				link = new PivotJoint(b1, b2, b1.worldPointToLocal(pivotJoints[i]), b2.worldPointToLocal(pivotJoints[i]));
				link.space = space;
			}
			*/
			
			for (i = 0; i < 21; i++)
			{
				initBodies();
			}
			
			function sortMarkersByPos(o0:Object, o1:Object):int 
			{ 
				return o0.pos - o1.pos;
			}
			scrollMarkers.sort(sortMarkersByPos);
			zoomMarkers.sort(sortMarkersByPos);
			
			function sortObjVec2(o0:Object, o1:Object):int
			{
				return o0.pos.x - o1.pos.x;
			}
			if (ropeMarkers != null)
			{
				ropeMarkers.sort(sortObjVec2);
			}
			chainMarkers0.sort(sortObjVec2);
			
			function numSort(a:uint, b:uint):int 
			{ 
				return a - b; 
			}
			recoveryMarkers.sort(numSort);
			clearMarkers.sort(numSort);
			
			function sortBodies(b0:Body, b1:Body):int
			{
				return (b0.position.x - b0.bounds.width / 2) - (b1.position.x - b1.bounds.width / 2);
			}
			allBodies.sort(sortBodies);
			fireBalls.sort(sortBodies);
			
			function sortVec2(v0:Vec2, v1:Vec2):int
			{
				return v0.x - v1.x;
			}
			limitScrollBottom.sort(sortVec2);
			limitScrollTop.sort(sortVec2);
			
			//Main.main.debug.addChild(debug.display);
			
			var stars:Stars = new Stars();
			stars.x = 0;
			stars.start();
			Starling.juggler.add(stars);
			layer0.addChild(stars);
			
			
			
			/*var edge:Edge = tmpBody.shapes.at(0).castPolygon.edges.at(0);
			var a = Vec2.get((edge.localVertex1.x + edge.localVertex2.x) / 2, (edge.localVertex1.y + edge.localVertex2.y) / 2);
			edge = tmpBody.shapes.at(0).castPolygon.edges.at(4);
			var b = Vec2.get((edge.localVertex1.x + edge.localVertex2.x) / 2, (edge.localVertex1.y + edge.localVertex2.y) / 2);
			
			tmpBody.allowRotation
			
			setTimeout(function():void
			{
				var bl:BodyList = BodySplitter.splitObj(tmpBody, a, b);
				tmpBody.space = null;
				
				bl.foreach(function(prt:Body):void
				{
					prt.space = space;
					prt.allowMovement = prt.allowRotation = false;
					//prt.position.setxy(500, 300);
				});
			}, 4000);*/
			
			
		}
		
		private function body_spring(cb:InteractionCallback):void
		{
			var b:Body = cb.int2.castBody;
			
			if (b.userData.addToRuntime != null && b.userData.link0.frequency == b.userData.frq * 14 && !b.userData.oscilator.isActive)
			{
				b.userData.addToRuntime = null;
				
				springingBodies.push(b); trace("pu");
				b.userData.link0.frequency = b.userData.frq;
				
				if (b.userData.link0.frequency == b.userData.frq)
				{
					b.userData.graphic.visible = true;
					layer3.removeChild(b.userData.graphic1);
				}
			}
		}
		
		private function hero_fatal(cb:InteractionCallback):void
		{
			var b2:Body = cb.int2.castBody;
			
			if (b2.userData.parent is Pursuer)
			{
				b2.userData.parent.attack();
			}
		}
		
		
		private function hero_bonus(cb:InteractionCallback):void
		{
			var b2:Body = cb.int2.castBody;
			
			switch(uint(b2.userData.type))
			{
				case 0: 
					hero.carmaLife++; 
					break;
				case 2: 
					hero.activateArmor(); 
					break;
				case 3: 
					hero.state = 3;
					break;
				case 4: 
					hero.state = 4;
					break;
			}
			
			clearBody(b2, false);
			
			//hero.dieTimer = 14;
			//uVec2.setxy(1200, 200);
			//hero.born(uVec2);
		}
		
		private function hero_fireball(cb:InteractionCallback):void
		{
			gameOver(false, 1);
		}
		
		private function ground_fireball(cb:InteractionCallback):void
		{
			play(s_fireBall);
			var b:Body = cb.int2.castBody;
			
			setTimeout(function():void
			{
				clearBody(b, false);
			}, 2000 + uint(Math.random() * 1400));
		}
		
		private function hero_bomb(cb:InteractionCallback):void
		{
			var b:Body = cb.int2.castBody;
			
			b.cbTypes.clear();
			
			setTimeout(function():void
			{
				bumCameraEffect();
				if (Mut.distV(b.position, hero.center) <= b.bounds.width)
				{
					gameOver(false, 1);
				}
			}, b.userData.explTime);
		}
		
		private function hero_jump(cb:InteractionCallback):void
		{
			var b:Body = cb.int2.castBody;
			var r:Number = b.rotation - Math.PI / 2;
			var force:uint = cb.int2.castBody.userData.pushForce;
			
			switch(hero.state)
			{
				case 2:
					hero.armor.velocity.setxy(0, 0);
					hero.armor.angularVel = 0;
					uVec2.setxy(force * Math.cos(r), force * Math.sin(r));
					hero.armor.applyImpulse(uVec2);
					b.cbTypes.remove(CB_JUMP);
			}
		}
		
		private function hero_bulb(cb:InteractionCallback):void
		{
			var b:Body = cb.int2.castBody;
			b.userData.lifeTime = bulbsLifeAfterTouch + uint(Math.random() * bulbsLifeAfterTouch);
			b.cbTypes.remove(CB_BULB);
		}
		
		private function hero_groundO(cb:InteractionCallback):void
		{
			if (!hero.onGround) 
			{
				hero.onGround = true;
			}
		}
		
		private function hero_groundE(cb:InteractionCallback):void
		{
			hero.onGround = false;
			
			if (hero.nextJumpTime == 0)
			{
				hero.jumpAbleTime = 7;
			}
		}
		
		private function hero_rope(cb:InteractionCallback):void
		{
			if (hero.clingConstr.space != null || hero.clingBody != null)
			{
				return;
			}
			
			hero.chainBody = cb.int2.castBody;
			hero.clingBody = cb.int1.castBody;
		}
		
		private function hero_ropeE(cb:InteractionCallback):void
		{
			if (hero.clingConstr.space != null)
			{
				return;
			}
			
			var b1:Body = cb.int1.castBody;
			
			if (b1 == hero.clingBody)
			{
				hero.clingBody = null;
				hero.chainBody = null;
			}
		}
		
		private function hero_sq(cb:InteractionCallback):void
		{
			var b1:Body = cb.int1.castBody;
			uVec2.setxy(0, 21);
			b1.applyImpulse(uVec2);
			
			if (hero.sqForce == 0) 
			{
				hero.sqForce++;
			}
		}
		
		private function hero_sqE(cb:InteractionCallback):void
		{
			if (hero.sqForce > 0) 
			{
				hero.sqForce--;
			}
		}
		
		private function hero_danger(cb:InteractionCallback):void
		{
			if (!hero.danger) hero.danger = true;
		}
		private function hero_dangerE(cb:InteractionCallback):void
		{
			hero.danger = false;
		}
		
		private function hero_persecutor(cb:InteractionCallback):void
		{
			cb.int2.castBody.userData.parentObj.attack();
		}
		
		private function press_ground(cb:InteractionCallback):void
		{
			var b1:Body = cb.int1.castBody;
			b1.userData.parentObj.interruptMovement = true;
		}
		
		private function hero_fall(cb:InteractionCallback):void
		{
			var b2:Body = cb.int2.castBody;
			if (b2.userData.touched || (hero.state != 2 && b2.userData.armored)) return;
			
			
			var bList:BodyList = new BodyList();
			
			if (b2.compound != null)
			{
				bList = b2.compound.bodies;
			}
			else 
			{
				bList.add(b2);
			}
			
			bList.foreach(function(nextBody:Body):void
			{
				if (nextBody.userData.link != null) 
				{
					nextBody.userData.link.space = space;
				}
				
				nextBody.userData.touched = true;
				nextBody.allowRotation = true; 
				nextBody.allowMovement = true;
				
				var fallTime:uint = nextBody.userData.fallTime;
				
				if (hero.state == 2)
				{
					if (fallTime == 0)// && Mut.distV(hero.center, nextBody.position) < 100)
					{
						nextBody.angularVel = Math.random() * 42 - 21;
					}
					
					hero.armor.velocity.setxy(hero.direction * 1400, hero.armor.velocity.y);
				}
				
				if (fallTime == 0) 
				{
					setTimeout
					(
						function ():void 
						{ 
							nextBody.shapes.at(0).sensorEnabled = true;
							
						}, 70
					);
					//nextBody.shapes.at(0).sensorEnabled = true;
					return;
				}
				
				setTimeout
				(
					function ():void 
					{ 
						nextBody.velocity.setxy(0, 0);
						nextBody.angularVel = 0; 
						nextBody.allowRotation = false; 
						nextBody.allowMovement = false; 
						
						shiver1();
						
						//zoomOscillator.setup(21, 1 / 10, l3m.a * 100, 3, 2, .5);
						//zoomOscillator.start();
						
					}, fallTime / 2
				);
				
				setTimeout(function ():void 
				{ 
					nextBody.allowRotation = true; 
					nextBody.allowMovement = true; 
					
					uVec2.setxy(0, Math.random() * 21);
					nextBody.applyImpulse(uVec2);
					nextBody.angularVel = 3.5 - Math.random() * 7;
					
				}, fallTime);
				
				if (nextBody.userData.link != null) setTimeout(function ():void { nextBody.userData.link.space = null; }, fallTime + 350);
			});
		}
		
		private function hero_press(cb:InteractionCallback):void
		{
			var b:Body = cb.int2.castBody;
			hero.checkDestrImpulses(b);
		}
		
		private function hero_spall(cb:InteractionCallback):void
		{
			var b:Body = cb.int2.castBody;
			if (b.userData.fallDistance == 0)
			{
				b.userData.fallDistance = 1;
				b.allowMovement = b.allowRotation = true;
				return;
			}
			if (b.velocity.length > 210) 
			{
				hero.checkDestrImpulses(b, 1400);
				if (b.userData.timeout != null)
				{
					b.userData.timeout = true;
					setTimeout(function():void
					{
						b.cbTypes.remove(CB_SPALL);
					}, 1000);
				}
			}
		}
		
		
		
		public function bumCameraEffect():void
		{
			zoomOscillator.setup(null, null, 1 / 10, l3m.a * 100, 3, 2, .5);
			dtYOscillator.setup(null, null, 1 / 4, 0, 42, 2, .5);
			dtXOscillator.setup(null, null, 1 / 2, 0, 21, 2, .5);
			
			zoomOscillator.start();
			dtYOscillator.start();
			dtXOscillator.start();
		}
		
		private function onKu(e:KeyboardEvent):void
		{
			if (!realGameStarted) 
			{
				return;
			}
			if (e.keyCode == 37)
			{
				controllLeft = false
			}
			else if (e.keyCode == 39)
			{
				controllRight = false
			}
			else if (e.keyCode == Keyboard.SPACE)
			{
				controllUp = false;
			}
		}
		
		private function onKd(e:KeyboardEvent):void
		{
			if (!realGameStarted) 
			{
				return;
			}
			
			if (e.keyCode == 37 && !controllLeft)  
			{
				controllLeft = true
			}
			else if (e.keyCode == 39 && !controllRight) 
			{
				controllRight = true
			}
			else if (e.keyCode == Keyboard.SPACE && !controllUp) 
			{
				controllUp = true;
			}
			else if (e.keyCode == Keyboard.A) 
			{
				hero.setCurrentBody(false);
			}
			else if (e.keyCode == Keyboard.S) 
			{
				hero.setCurrentBody(true);
			}
			
		}
		
		private function onTouch(e:TouchEvent):void
        {
			if (pause || !realGameStarted) 
			{
				return; 
			}
			
			var touches:Vector.<Touch> = e.getTouches(stage);
			
            for (var i:uint = 0; i < touches.length; i++)
			{
				var touch:Touch = touches[i];
				if(touch)
				{
					var tx:Number = touch.globalX * Main.main.scaleFactor;
					var ty:Number = touch.globalY * Main.main.scaleFactor;
					
					if (tx < Main.main.gameWidth / 2)
					{
						if(touch.phase == TouchPhase.BEGAN)
						{
							previousTouch.x = tx;
							
							if (tx < 100)
							{
								if (!controllLeft)
								{
									controllLeft = true;
									if (controllRight)
									{
										controllRight = false;
										//hero.force = 7;
									}
								}
							}
							else
							{
								if (!controllRight)
								{
									controllRight = true;
									if (controllLeft)
									{
										controllLeft = false;
										//hero.force = 7;
									}
								}
							}
						}
						else if(touch.phase == TouchPhase.ENDED)
						{
							controllLeft = false;
							controllRight = false;
						}
						else if(touch.phase == TouchPhase.MOVED)
						{
							/*var deltaTouchX:Number = previousTouch.x - tx;
							previousTouch.x = tx;
							
							if (Math.abs(deltaTouchX) > 7)
							{
								if (deltaTouchX > 0)
								{
									if (!controllLeft)
									{
										controllLeft = true;
										if (controllRight)
										{
											controllRight = false;
											//hero.force = 7;
										}
									}
								}
								else if (!controllRight)
								{
									controllRight = true;
									if (controllLeft)
									{
										controllLeft = false;
										//hero.force = 7;
									}
								}
							}*/
						}
					}
					else if(touch.phase == TouchPhase.BEGAN)
					{
						if (Mut.dist(tx, ty, pauseB.x, pauseB.y) < 42 && !pause && pauseB.parent != null)
						{
							Main.main.pauseA();
							pause = true;
							pauseActuate();
							return;
						}
						controllUp = true;
					}
					else if (touch.phase == TouchPhase.ENDED) 
					{
						controllUp = false;
					}
				}
			}
        }
		
		public function delay(cb:Function, del:uint):void
		{
			var d:Delay = delayVectorOff.shift();
			d.start(cb, del);
			delayVectorOn.push(d);
		}
		
		public function clear(quit:Boolean = false):void
		{
			if (ropes != null)
			{
				ropes.length = 0;
			}
			
			if (activeRopes != null)
			{
				activeRopes.length = 0;
			}
			
			if (ropeMarkers != null)
			{
				ropeMarkers.length = 0;
			}
			
			delayVectorOn.length = 0;
			oscilators.length = 0;
			clearMarkers.length = 0;
			softBodies.length = 0;
			chainLinksPool.length = 0;
			pivotJointsPool.length = 0;
			chainMarkers0.length = 0;
			zoomMarkers.length = 0;
			recoveryMarkers.length = 0;
			scrollMarkers.length = 0;
			limitScrollBottom.length = 0;
			limitScrollTop.length = 0;
			bottomLim = Infinity;
			topLim =-Infinity;
			controlledObj.length = 0;
			rotatingBodies.length = 0;
			springingBodies.length = 0;
			allBodies.length = 0;
			bulbEmitters.length = 0;
			bulbs.length = 0;
			fireBalls.length = 0;
			spalls.length = 0;
			nextToClearCheck = 0;
			
			bodyToClear.clear();
			
			l3m.a = l3m.d = l2m.a = l2m.d = l1m.a = l1m.d = 1;
			l3m.tx = l3m.ty = l2m.tx = l2m.ty = l1m.tx = l1m.ty = 0;
			
			cameraFollowPointX = -400;
			
			targetVec.setxy(0, 0);
			stepY = stepX = 0;
			
			cameraDeltaX = 0;
			cameraDeltaY = 0;
			
			deltaScroll.setxy(0, 0);
			
			jumpAble = true;
			controllDown = controllLeft = controllRight = controllUp = false;
			
			realGameStarted = false;
			
			zoomScaleOld = 1;
			
			zoomOscillator.time = zoomOscillator.timeLim + 1;
			dtXOscillator.time = dtXOscillator.timeLim + 1;
			dtYOscillator.time = dtYOscillator.timeLim + 1;
			
			Starling.juggler.purge();
			
			for (var i:uint = 0; i < numLayers; i++) 
			{
				var l:Sprite = this["layer" + i];
				
				while (l.numChildren > 0)
				{
					var child:DisplayObject = l.getChildAt(0);
					if (child is Image) Image(child).texture.dispose()
					else if (child is MovieClip) MovieClip(child).texture.dispose();
					l.removeChild(child);
				}
				
				l.removeChildren(0, -1, true);
			}
			
			space.clear();
			
			Actuate.timer(.4).onComplete(function():void { System.pauseForGCIfCollectionImminent(0.14); } );
			if (!quit) Actuate.timer(1.4).onComplete(function():void { startGame(currentLevel); } )
			else Main.main.addChild(Main.main.mainMenu);
			
			if (pauseB.parent != null) pauseB.parent.removeChild(pauseB);
		}
		
		public function gameOver(quit:Boolean = false, dieCase:uint = 0):void
		{
			hero.dying(dieCase); //clear(false); return;
			
			//if (pauseB.parent != null) pauseB.parent.removeChild(pauseB);
			
			if (dieCase != 0)
			{
				addChild(blackSquare);
				Actuate.tween(blackSquare, 1.4, { alpha:1 } ).ease(Linear.easeNone).onComplete(function():void
				{
					clear(false);
				});
			}
			else
			{
				clear(quit);
			}
		}
		
		private function pauseActuate():void
		{
			Actuate.pause(deltaScroll);
			Actuate.pause(l3m);
			Actuate.pause(blackSquare);
		}
		
		public function resumeActuate():void
		{
			Actuate.resume(deltaScroll);
			Actuate.resume(l3m);
			Actuate.resume(blackSquare);
		}
		
		private function shiver1():void
		{
			/*dtYOscillator.setup(7000, 1 / 2, 0, 7, 4, .4);
			dtXOscillator.setup(7000, 1 / 2, 0, 7, 4, .4);
			dtYOscillator.start();
			dtXOscillator.start();*/
		}
		
		public function tentacleStartEffect(pos:Vec2):void
		{
			tentacleStartEmmiter.emitStart(pos.x, pos.y, 1);
		}
		
		public function	play(sound:Sound):void
		{
			if (Main.main.soundEnable) 
			{
				sound.play();
			}
		}
		
		private function enterFrame(e:Event):void
		{
			
			if (pause || hero == null) 
			{
				return;
			}
			
			for each(var o:Oscillator in oscilators)
			{
				o.update();
			}
			
			for each(var d:Delay in delayVectorOn)
			{
				d.update();
			}
			
			/*if (!zoomOscillator.paused) 
			{
				l3m.a = l3m.d = zoomOscillator.update() / 100;
			}
			
			if (!dtYOscillator.paused) 
			{
				cameraDeltaY = dtYOscillator.update();
			}
			
			if (!dtXOscillator.paused) 
			{
				cameraDeltaX = dtXOscillator.update();
			}*/
			
			//camera
			
			var cFollowPointX:Number;
			cFollowPointX = cameraFollowPointX = hero.center.x;
			
			var zoomStep:Number = l3m.a - zoomScaleOld;
			
			if (!hero.isDead)
			{
				var stepDivider:uint = 21;
				if (!realGameStarted) stepDivider = 1;
				
				stepX = (cFollowPointX - targetVec.x) / stepDivider;
				
				if (hero.center.y >= bottomLim && (hero.center.y - targetVec.y) > 0)
				{
					stepY = (bottomLim - targetVec.y);
				}
				else if (hero.center.y <= topLim && (hero.center.y - targetVec.y) < 0)
				{
					stepY = (topLim - targetVec.y);
				}
				else
				{
					stepY = (hero.center.y - targetVec.y);
				}
				
				if (hero.center.y > borderBottom + 70 && realGameStarted) 
				{
					gameOver(false, 1);
				}
				
				stepY /= stepDivider;
			}
			else
			{
				if (Math.abs(stepX) > .001) stepX *= .95;
				if (Math.abs(stepY) > .001) stepY *= .95;
			}
			
			targetVec.x += stepX;
			targetVec.y += stepY;
			
			var targetX:Number = -targetVec.x + Main.main.gameWidth  / 2 - deltaScroll.x + cameraDeltaX;
			var targetY:Number = -targetVec.y + Main.main.gameHeight / 2 - deltaScroll.y + cameraDeltaY;
			
			l3m.tx = targetX + targetVec.x * (1 - l3m.a);
			l3m.ty = targetY + targetVec.y * (1 - l3m.d);
			
			l2m.a += zoomStep / 2;
			l2m.d += zoomStep / 2;
			l2m.tx = targetX / 3 + targetVec.x * (1 - l2m.a) / 3;
			l2m.ty = targetY / 5 + targetVec.y * (1 - l2m.d) / 5;
			//l2m.tx = l3m.tx / 2.1;
			//l2m.ty = l3m.ty / 5;
			
			l1m.a += zoomStep / 3.5;
			l1m.d += zoomStep / 3.5;
			l1m.tx = targetX / 5 + targetVec.x * (1 - l1m.a) / 5;
			l1m.ty = targetY / 8 + targetVec.y * (1 - l1m.d) / 8;
			//l1m.tx = l3m.tx / 5;
			//l1m.ty = l3m.ty / 8;
			
			/*l2m.d = l3m.d / 1.4;
			l2m.a = l3m.a / 1.4;
			l2m.tx = l3m.tx / 2;
			l2m.ty = l3m.ty / 4;
			
			l2m.a = l3m.a / 2;
			l2m.d = l3m.d / 2;
			l2m.tx = l3m.tx / 4;
			l2m.ty = l3m.ty / 7;*/
			
			//Main.main.debug.transform.matrix = l3m.clone(); debug.draw(space); debug.flush(); debug.clear();
			
			zoomScaleOld = l3m.a;
			
			checkLim(layer2);
			checkLim(layer1);
			
			for each(var b:Body in rotatingBodies)
			{
				if (b.space != null)
				{
					b.angularVel = b.userData.angVel;
				}
			}
			
			for each(b in springingBodies)
			{
				var delta:Number = Math.abs(b.rotation - b.userData.startRotation);
				
				if (delta > 1 / 4)
				{
					
					b.userData.link0.frequency += .2;
					
					if (delta > 1 / 3)
					{
						var g0:Image = b.userData.graphic;
						var g1:Image = b.userData.graphic1;
						
						uVec2 = b.localPointToWorld(b.userData.graphicOffset1);
						g1.rotation = g0.rotation;
						g1.x = uVec2.x;
						g1.y = uVec2.y;
						layer3.addChild(g1);
						g0.visible = false;
						
						b.userData.link0.frequency = b.userData.frq * 21;
						b.userData.addToRuntime = b;
						
						springingBodies.removeAt(springingBodies.indexOf(b));
						
						var tempBody:Body = b;
						delay(function():void
						{
							trace("app");
							tempBody.userData.link0.frequency = tempBody.userData.frq * 14;
							tempBody.velocity.setxy(0, 0);
							tempBody.rotation = tempBody.userData.startRotation;
						}, 42);
						
						delay(function():void
						{
							
							
							hero.applyVel(b.velocity.mul(7) );
							hero.looseControllTime = 60; 
							
							//delay(function():void
							//{
								tempBody.userData.oscilator.setup(tempBody.userData.graphic1, "rotation", 1/2, tempBody.userData.startRotation, 1 / 5);
								tempBody.userData.oscilator.start();
							//}, 14);
							
						}, 14);
						
					}
				}
			}
			
			
			
			for each(var cObj:ControlledObj in controlledObj) 
			{
				cObj.enterFrame();
			}
			
			for each(var bulbEm:Object in bulbEmitters)
			{
				if (bulbEm.pos.x - hero.center.x > 0 && bulbEm.pos.x - hero.center.x < 2100)
				{
					if (bulbEm.delay > 0) 
					{
						bulbEm.delay--
					}
					else
					{
						bulbEm.delay = 140 + uint(Math.random() * 140);
						var tb:Body = makeBody(bulbEm.pos.x, bulbEm.pos.y, new Circle(55));
						tb.cbTypes.add(CB_GROUND);
						tb.cbTypes.add(CB_BULB);
						tb.gravMass *=-1 / (35 - uint(Math.random() * 7));
						//img.pivotX = img.width / 2;
						//img.pivotY = img.height / 2;
						tb.space = space;
						
						tb.userData.graphic = new Image(atlas.getTexture("bulb"));
						layer3.addChild(tb.userData.graphic);
						tb.userData.graphic.pivotX = tb.userData.graphic.width / 2;
						tb.userData.graphic.pivotY = tb.userData.graphic.width / 2;
						tb.userData.lifeTime = 700;
						
						bulbs.push(tb);
					}
					
				}
			}
			
			for each(b in bulbs)
			{
				if (b.userData.lifeTime > 0) 
				{
					b.userData.lifeTime--
				}
				else
				{
					clearBody(b, false);
					bulbs.removeAt(bulbs.indexOf(b));
				}
			}
			
			for (var i:uint = 0; i < softBodies.length; i++) 
			{
                if (softBodies[i].body.space == null) 
				{
					continue;
				}
				var sb:Compound = softBodies[i].body;
                var pressure:Number = 16 * 0.001 * (sb.userData.area - softBodies[i].polygonalArea(sb)); //pressure = 100;
				var refEdges:Vector.<Edge> = sb.userData.refEdges;
				
				for (var j:uint = 0; j < refEdges.length; j++) {
                    var edge:Edge= refEdges[j];
                    var body:Body = edge.polygon.body;
					
					body.applyImpulse(edge.worldNormal.mul(pressure * sb.userData.pressureMul, true), body.position, true);
                }
            }
			
			for each(var rope:SoftBodyNape in activeRopes)
			{
				rope.update();
			}
			
			for (i = 0; i < space.liveBodies.length; i++) 
			{
				body = space.liveBodies.at(i);
				
				
				var graphic:DisplayObject = body.userData.graphic;
				if (graphic == null) continue;
				
				var graphicOffset:Vec2 = body.userData.graphicOffset;
				if (!graphicOffset) 
				{
					uVec2.setxy(0, 0);
					graphicOffset = uVec2;
				}
				uVec2 = body.localPointToWorld(graphicOffset);
				
				/*var dx:Number = 0;
				var dy:Number = 0;
				if (body.userData.dt)
				{
					dx = body.userData.dt.x;
					dy = body.userData.dt.y;
				}*/
				graphic.x = uVec2.x// + dx;
				graphic.y = uVec2.y// + dy;
				//if (body.userData.blockRotation != null) 
				graphic.rotation = body.rotation;
				
				if (body.userData.sensorBody != null)
				{
					body.userData.sensorBody.position.set(body.position);
					body.userData.sensorBody.rotation = body.rotation;
				}
			}
			
			if(emitters != null) for each(var prt:ParticlesEm in emitters)
			{
				if (prt.eTimes > 0 || prt.particles.length > 0) 
				{
					prt.onEnterFrame()
				}
				else if (prt.toRemove) 
				{
					emitters.removeAt(emitters.indexOf(prt));
				}
			}
			
			space.step(1 / frameRate, 15, 15);
			
			if (hero.isDead)
			{
				return;
			}
			
			initBodies();
			checkChains();
			checkRopeMarkers();
			
			clearBodies();
			checkToClear();
			
			utilCounter--;
			switch(utilCounter)
			{
				case 0:
					utilCounter = 4;
					break;
				case 1:
					checkTopLim();
					checkBottomLim();
					break;
				case 2:
					checkForFireBalls();
					break;
				case 3:
					scrollMarkersCheck();
					zoomMarkersCheck();
					recoveryMarkersCheck();
			}
			
			//controls
			
			if (controllLeft)
			{
				hero.turn(LEFT);
			}
			else if (controllRight)
			{
				hero.turn(RIGHT);
			}
			else 
			{
				if (hero.direction == LEFT || hero.direction == RIGHT) hero.turn(STOP);
			}
			
			if (controllUp)
			{
				switch(hero.state)
				{
					case 4:
						
						if (hero.sb.softPolygon != hero.sb.ball)
						{
							hero.setCurrentBody(false);
							hero.jump();
						}
						break;
					case 0:
					case 2:
					case 3:
						if (jumpAble) 
						{
							hero.jump();
							jumpAble = false;
							//zoomOscillator.time = 0;
						}
						break;
					case 1:
						hero.fly();
						break;
				}
			}
			else 
			{
				if (hero.state == 4 && hero.sb.softPolygon == hero.sb.ball)
				{
					hero.setCurrentBody(true);
				}
				if (!jumpAble)
				{
					jumpAble = true;
				}
			}
			
			//borders_______________________________________________________________________________________________
			
			borderRight = hero.center.x + (Main.main.gameWidth / 2 + deltaScroll.x) / l3m.a - (hero.center.x - targetVec.x);
			borderLeft = hero.center.x - (Main.main.gameWidth / 2 - deltaScroll.x) / l3m.a - (hero.center.x - targetVec.x);
			borderBottom = hero.center.y + (Main.main.gameHeight / 2 + deltaScroll.y) / l3m.a - (hero.center.y - targetVec.y);
			center = borderRight - Main.main.gameWidth / 2 / l3m.a;
			
			//hero______________________________________________
			
			hero.run();
			
			//utilFunctions_____________________________________________________________________
			
			function checkLim(obj:Sprite):void
			{
				if (obj.numChildren == 0) return;
				
				var firstChild:DisplayObject = obj.getChildAt(0);
				var px:Number = obj.transformationMatrix.tx + firstChild.x;
				
				if (px <= -2100 + (obj.transformationMatrix.tx - obj.transformationMatrix.tx / obj.transformationMatrix.a))
				{
					for (var i:uint = 0; i < obj.numChildren; i++)
					{
						obj.getChildAt(i).x += 2100;
					}
				}
			}
			
			function checkBottomLim():void
			{
				if (limitScrollBottom.length == 0)
				{
					return;
				}
				
				var l:Vec2 = limitScrollBottom[0];
				
				if (l.x < hero.center.x)
				{
					bottomLim = l.y;
					limitScrollBottom.shift();
				}
			}
			
			function checkTopLim():void
			{
				if (limitScrollTop.length == 0)
				{
					return;
				}
				
				var l:Vec2 = limitScrollTop[0];
				
				if (l.x < hero.center.x)
				{
					topLim = l.y;
					limitScrollTop.shift();
				}
			}
			
			function scrollMarkersCheck():void
			{
				if (scrollMarkers.length > 0)
				{
					var o:Object = scrollMarkers[0];
					
					if (o.pos < hero.center.x)
					{
						currentDeltaScrollInd = scrollMarkers.indexOf(o);
						
						if (o.dt.x != deltaScroll.x) 
						{
							Actuate.tween(deltaScroll, 2.1, { x:Main.main.gameWidth * o.dt.x / 100 } ).ease(Sine.easeInOut);
						}
						
						if (o.dt.y != deltaScroll.y) 
						{
							Actuate.tween(deltaScroll, 2.1, { y:Main.main.gameHeight * o.dt.y / 100 } ).ease(Sine.easeInOut);
						}
						
						scrollMarkers.shift();
					}
				}
			}
			
			function zoomMarkersCheck():void
			{
				if (zoomMarkers.length > 0)
				{
					var o:Object = zoomMarkers[0];
					
					if (o.pos < hero.center.x)
					{
						Actuate.tween(l3m, o.time, { a:o.val, d:o.val } ).ease(Sine.easeOut);
						currentZoom = o.val;
						zoomMarkers.shift();
						return;
					}
				}
			}
			
			function recoveryMarkersCheck():void
			{
				if (recoveryMarkers.length > 0)
				{
					if (recoveryMarkers[0] < hero.center.x && hero.onGround)
					{
						recoveryPoint.setxy(recoveryMarkers[0], hero.center.y - 35);
						deltaScrollStartInd = currentDeltaScrollInd;
						recoveryMarkers.shift();
						
						save();
					}
				}
			}
			
			function checkRopeMarkers():void
			{
				if (ropeMarkers == null || ropeMarkers.length == 0)
				{
					return;
				}
				
				var rm:Object = ropeMarkers[0];
				
				if (rm.pos.x <= borderRight + CLEARANCE_L)
				{
					var rope:SoftBodyNape = ropes.pop();
					rope.activate(null, rm.pos);
					activeRopes.push(rope);
					ropeMarkers.removeAt(ropeMarkers.indexOf(rm));
				}
			}
			
			function checkForFireBalls():void
			{
				if (fireBalls.length == 0)
				{
					return;
				}
				
				var b:Body = fireBalls[0];
				{
					if (b.position.x - hero.center.x < 7)
					{
						b.space = space;
						layer3.addChild(b.userData.graphic);
						
						if (b.userData.childBalls != null)
						{
							while (b.userData.childBalls.length != 0)
							{
								var child:Body = b.userData.childBalls.pop();
								child.space = space;
								layer3.addChild(child.userData.graphic);
							}
						}
						
						fireBalls.shift();
					}
				}
			}
			
			function clearBodies():void
			{
				if (!bodyToClear.empty())
				{
					var cBody:Body = bodyToClear.pop();
					clearBody(cBody);
					return;
				}
			}
			
			function checkToClear():void
			{
				if (hero.isDead || nextToClearCheck >= space.bodies.length)
				{
					return;
				}
				
				var body:Body = space.bodies.at(nextToClearCheck);
				nextToClearCheck == space.bodies.length - 1 ? nextToClearCheck = 0 : nextToClearCheck++;
				
				if (body.cbTypes.at(1) == CB_HERO)
				{
					return;
				}
				
				if(body.position.x + body.bounds.width / 2 + CLEARANCE_L < borderLeft)
				{
					bodyToClear.push(body);
					return;
				}
				
				if (body.type == BodyType.DYNAMIC && body.velocity.y > 70 && body.position.y > borderBottom + CLEARANCE_B)
				{
					bodyToClear.push(body);
				}
			}
			
			function checkChains():void
			{
				if (chainMarkers0.length == 0)
				{
					return;
				}
				
				var ch:Object = chainMarkers0[0];
				
				if (ch.pos.x <= borderRight + CLEARANCE_R)
				{
					makeChain(ch.len, ch.pos, ch.end);
					chainMarkers0.shift();
				}
			}
			
			function makeChain(len:uint, pos:Vec2, end:Vec2):void
			{
				var tempBody:Body;
				var pj:PivotJoint;
				
				for (var i:uint = 0; i < len; i++)
				{
					if (tempBody == null)
					{
						var tempBodyList:BodyList = space.bodiesInCircle(pos, 4);
						
						if (tempBodyList.length != 0) 
						{
							tempBody = tempBodyList.at(0);
						}
						else
						{
							tempBody = space.world;
						}
					}
					
					pj = pivotJointsPool.pop();
					
					pj.body1 = tempBody;
					
					
					if (tempBody.type == BodyType.STATIC || !tempBody.allowMovement || tempBody == space.world) 
					{
						uVec2.set(pos);
						pj.anchor1.set (tempBody.worldPointToLocal(pos));
					}
					else 
					{
						pj.anchor1.setxy(0, 15);
						uVec2.setxy(tempBody.position.x, tempBody.position.y + 30);
					}
					tempBody = chainLinksPool.pop();
					tempBody.position.set(uVec2);
					tempBody.space = space;
					if(tempBody.userData.graphic.parent == null) layer3.addChild(tempBody.userData.graphic);
					
					pj.body2 = tempBody;
					pj.anchor2.setxy(0, -15);
					
					pj.space = space;
				}
				
				if (end != null)
				{
					
					pj = pivotJointsPool.pop();
					pj.body1 = tempBody;
					pj.anchor1.setxy(0, 15);
					
					tempBody = space.bodiesInCircle(end, 4).at(0);
					pj.body2 = tempBody;
					pj.anchor2.set(tempBody.worldPointToLocal(end));
					pj.space = space;
				}
			}
		}
		
		private function initBodies():Boolean
		{
			if (allBodies.length == 0)
			{
				return false;
			}
			
			var b:Body = allBodies[0];
			var inited:Boolean = false;
			
			if(b.position.x - b.bounds.width / 2 <= borderRight + CLEARANCE_R)
			{
				inited = true;
				b.space = space;
				
				if (b.userData.rotated)
				{
					setTimeout(function():void
					{
						rotatingBodies.push(b);
					}, 700);
				}
				
				if (b.userData.sensorBody != null) 
				{
					b.userData.sensorBody.space = space;
				}
				if (b.userData.graphic != null) 
				{
					layer3.addChild(b.userData.graphic);
				}
				
				i--;
				
				if (b.userData.link0 != null || b.userData.link1 != null)
				{
					var lnk;
					
					if (b.userData.link0 != null)
					{
						lnk = b.userData.link0;
					}
					else
					{
						lnk = b.userData.link1;
					}
					
					space.bodiesInCircle(b.localPointToWorld(lnk.anchor1, true), 14).foreach(function(bd:Body):void
					{
						if (bd != b)
						{
							if (b.userData.link0 != null)
							{
								b.userData.link0.body2 = bd;
								b.userData.link0.anchor2 = bd.worldPointToLocal(b.userData.link0.anchor2);
								b.userData.link0.space = space;
							}
							
							if (b.userData.link1 != null)
							{
								b.userData.link1.body2 = bd;
								b.userData.link1.anchor2 = bd.worldPointToLocal(b.userData.link1.anchor2);
								b.userData.link1.space = space;
							}
							
							bd.group = noInterationGroup;
						}
					});
				}
				else if (b.userData.fallDistance != null && b.userData.fallDistance != 0) 
				{
					spalls.push(b);
				}
				
				allBodies.shift();
			}
			
			for (var i:uint = 0; i < spalls.length; i++)
			{
				b = spalls[i];
				
				if (Mut.distV(hero.center, b.position) < b.userData.fallDistance)
				{
					b.allowMovement = b.allowRotation = true;
					spalls.removeAt(spalls.indexOf(b));
					
					shiver1();
				}
			}
			
			return inited;
		}
	}
}