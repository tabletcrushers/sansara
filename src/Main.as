package
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.events.Event;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.media.Sound;
	import flash.net.SharedObject;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import starling.core.Starling;

	[SWF(width="400", height="300", frameRate="60", backgroundColor="#000000")]
	public class Main extends Sprite
	{
		public var debug:Sprite = new Sprite;
		
		public static var main:Main;
		
		public var scaleFactor:Number;
		public var gameWidth:Number;
		public var gameHeight:Number;
		public var aspectRatio:Number;
		
		public var mainMenu:MovieClip;
		public var pauseMenu:MovieClip;
		
		public var sharedObj:SharedObject = SharedObject.getLocal("sansara");
		
		public var soundEnable:Boolean = false;
		
		private var _starling:Starling;
		
		public function startGame(curLevel:uint):void
		{
			Game.game.startGame(curLevel);
			Starling.current.nativeStage.focus = Starling.current.nativeStage;
		}
		
		public function pauseA():void
		{
			addChild(pauseMenu);
		}
		
		public function pauseD(action:uint = 0):void
		{
			Game.game.pause = false;
			Game.game.resumeActuate();
			switch(action)
			{
				case 0:
					break;
				case 1:
					Game.game.gameOver();
					break;
				case 2:
					Game.game.gameOver(true);
			}
			Starling.current.nativeStage.focus = Starling.current.nativeStage;
		}
		
		public function Main()
		{
			addChild(debug);
			
			sharedObj.clear();
			
			
			
			main = this;
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			scaleFactor = 600 / stage.fullScreenHeight;
			gameWidth = stage.fullScreenWidth * scaleFactor;
			gameHeight = stage.fullScreenHeight * scaleFactor;
			
			aspectRatio = gameWidth / gameHeight;
			
			//MainMenu_______________________________________________________
			mainMenu = new Menu();
			mainMenu.scaleX /= scaleFactor;
			mainMenu.scaleY /= scaleFactor;
			mainMenu.x = stage.fullScreenWidth / 2;
			mainMenu.y = stage.fullScreenHeight / 2;
			addChild(mainMenu);
			mainMenu.totalLevels = 1;
			if (Main.main.sharedObj.size > 0) 
			{ 
				mainMenu.totalLevels = sharedObj.data.currentLevel;
			}
			
			//PauseMenu___________________________________________________
			pauseMenu = new Pause();
			pauseMenu.scaleX /= scaleFactor;
			pauseMenu.scaleY /= scaleFactor;
			pauseMenu.x = stage.fullScreenWidth / 2;
			pauseMenu.y = stage.fullScreenHeight / 2;
			
			//InitStarling____________________________________________________________________________________________
			_starling = new Starling(Game, stage, new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight), null, "auto", "baseline");
			_starling.showStats=true;
			_starling.start();
		}
	}
}