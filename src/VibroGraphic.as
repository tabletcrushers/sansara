package 
{
	import com.eclecticdesignstudio.motion.Actuate;
	import com.eclecticdesignstudio.motion.easing.Sine;
	import com.eclecticdesignstudio.motion.easing.Linear;
	import starling.display.Sprite;
	import nape.geom.Vec2;
	import starling.display.DisplayObject;
	
	import flash.utils.setTimeout;
	
	/**
	 * ...
	 * @author ...
	 */
	public class VibroGraphic extends Sprite
	{
		private var graphic:DisplayObject;
		private var interval:uint;
		private var sign:int = 1;
		private var signR:int = 1;
		private var time:Number;
		private var randomizeInterval:uint;
		private var randomizeTime:Number;
		
		public function VibroGraphic(graphic:DisplayObject, interval:uint = 7, randomizeInterval:uint = 14, time:Number = .3, randomizeTime:Number = .4):void
		{
			super();
			
			this.addChild(graphic);
			
			this.graphic = graphic;
			this.interval = interval;
			this.time = time;
			this.randomizeInterval = randomizeInterval;
			this.randomizeTime = randomizeTime;
		}
		
		public function init():void
		{
			vibrate();
			vibrateR();
		}
		
		private function vibrate():void
		{
			sign *=-1;
			Actuate.tween(graphic, time + randomizeTime * Math.random(), {y:(interval + randomizeInterval * Math.random()) * sign}).ease(Sine.easeInOut).onComplete(function():void
			{
				if (graphic.parent != null)
				{
					//setTimeout(vibrate, Math.random() * 400);
					vibrate();
				}
			});
		}
		
		private function vibrateR():void
		{
			signR *=-1;
			Actuate.tween(graphic, .4, {rotation: .4 * Math.random() * signR}).ease(Linear.easeNone).onComplete(function():void
			{
				if (graphic.parent != null)
				{
					//setTimeout(vibrate, Math.random() * 400);
					vibrateR();
				}
			});
		}
		
	}
	
}