package 
{
	import com.eclecticdesignstudio.motion.Actuate;
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;
	import nape.constraint.Constraint;
	import nape.phys.Compound;
	import utils.SoftPolygon;
	
	import nape.constraint.PivotJoint;
	import nape.dynamics.InteractionFilter;
	import nape.geom.AABB;
	import nape.geom.GeomPoly;
	import nape.geom.Ray;
	import nape.geom.RayResult;
	import nape.geom.Vec2List;
	import nape.geom.Vec3;
	import nape.phys.BodyList;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.phys.Body;
	import nape.geom.Vec2;
	import nape.shape.Polygon;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.textures.TextureAtlas;
	
	import utils.SegmentObjectNape;
	import utils.SoftBody;
	import utils.SoftBodyNape;
	import utils.Mut;
	
	
	
	public class Hero
	{
		public var danger:Boolean;
		
		public var state:uint = 0;
		
		[Embed(source="../Assets/character0.png")]
		public var characterTexture1:Class;
		
		
		public var sb:SoftBody;
		
		private var uVec2:Vec2 = new Vec2(0, -40);
		private var uRay:Ray = new Ray(uVec2, uVec2);
		private var uFilter:InteractionFilter = new InteractionFilter(2);
		
		public var nextJumpTime:uint = 0;
		private const jumpForce:int = 35;
		
		public var center:Vec2;
		
		public var direction:int = 0;
		
		private var catchAble:uint = 0;
		public var clingConstr:PivotJoint = null;
		private var rockAble:Boolean = true;
		private var rockTimer:uint;
		public var clingBody:Body;
		public var chainBody:Body;
		
		public var force:Number = 2;
		private const FORCE_LIM:uint = 21;
		private var postPosition:Vec2;
		private var limVelocityGround:uint = 420;
		private var limVelocityAir:uint = 280;
		
		private const RADIUS:uint = 35;
		
		public var afterJump:Boolean;
		
		public var dieTimer:uint = 0;
		private var bornTimer:uint = 0;
		public var carmaLife:uint = 0;
		
		public var onGround:Boolean;
		public var jumpAbleTime:uint = 0;
		
		public var sqForce:uint = 0;
		
		public var armor:Body;
		
		public var isDead:Boolean = false;
		
		private var tentacle:SoftBodyNape;
		[Embed(source="../assets/tentacle.png")]
		private var tentacleTexture:Class;
		
		public var tentacleEnabled:Boolean = false;
		
		public var velcros:BodyList = new BodyList();
		
		public var compoundBody:Compound;
		
		private var ballEnabe:uint = 0;
		
		public const FLY:uint = 1;
		public const BALL:uint = 4;
		public const TENTACLE:uint = 3;
		public const ARMOR:uint = 2;
		public const DEFAULT:uint = 0;
		
		public var looseControllTime:uint = 0;
		
		//4fly_____________________________________
		private var flyBody:Body;
		
		public function Hero():void
		{
			isDead = false;
			
			sb = new SoftBody
			(
				new SoftPolygon(new GeomPoly(Polygon.regular(RADIUS, RADIUS, 28)), new Vec2(250, 750), 14, 100, 42, 2),
				new SoftPolygon(new GeomPoly(Polygon.regular(RADIUS * 1.7, RADIUS * 1.7, 28)), new Vec2(250, 750), 14, 100, 42, 2),
				Texture.fromBitmap(new characterTexture1(), false)
			);
			
			Texture.fromBitmap(new characterTexture1(), false);
			
			center = sb.center;
			compoundBody = sb.softPolygon.body;
			
			/*compoundBody.visitBodies(function(b:Body):void
			{ 
				b.shapes.at(0).filter.sensorGroup = 2;
				b.cbTypes.add(Game.game.CB_HERO);
				b.group = Game.game.heroGroup;
				b.shapes.at(0).filter.collisionMask = ~2;
			});*/
			
			for (var i:uint = 0; i < sb.softPolygon.body.bodies.length; i++)
			{
				var b0:Body = compoundBody.bodies.at(i);
				var b1:Body = sb.ball.body.bodies.at(i);
				
				b0.shapes.at(0).filter.sensorGroup = 2;
				b0.cbTypes.add(Game.game.CB_HERO);
				b0.group = Game.game.heroGroup;
				b0.shapes.at(0).filter.collisionMask = ~2
				
				if (b1 == null)
				{
					continue;
				}
				
				b1.shapes.at(0).filter.sensorGroup = 2;
				b1.cbTypes.add(Game.game.CB_HERO);
				b1.group = Game.game.heroGroup;
				b1.shapes.at(0).filter.collisionMask = ~2
				
				b1.gravMass /= 14;
			}
			
			compoundBody.userData.pressureMul = 1;
			
			compoundBody.space = Game.game.space;
			//compoundBody.visitBodies(function(b) { b.gravMass = 0; } );
			Game.game.softBodies.push(sb.softPolygon);			
			Game.game.layer3.addChild(sb);
			Game.game.layer3.swapChildren(sb, Game.game.layer3.getChildAt(0));
			
			clingConstr = new PivotJoint(compoundBody.bodies.at(0), compoundBody.bodies.at(1), new Vec2(0, 0), new Vec2(0, 0));
			
			postPosition = new Vec2();
			postPosition.set(center);
			
			flyBody = compoundBody.bodies.at(0);
			
			setupTentacle();
		}
		
		public function setCurrentBody(isDefaultBody:Boolean = true):void
		{
			if (ballEnabe != 0 || isDead)
			{
				return;
			}
			
			ballEnabe = 21;
			
			var stash:SoftPolygon = sb.softPolygon;
			
			clearSoftBody();
			if (!isDefaultBody) 
			{
				sb.softPolygon = sb.ball;
			}
			else
			{
				sb.softPolygon = sb.defaultBody;
			}
			
			activateSoftBody();
			compoundBody = sb.softPolygon.body;
			
			for (var i:uint = 0; i < sb.softPolygon.body.bodies.length; i++)
			{
				var appearedBody:Body = sb.softPolygon.body.bodies.at(i);
				var stashedBody:Body = stash.body.bodies.at(i);
				
				appearedBody.position.set(stashedBody.position);
				appearedBody.velocity.set(stashedBody.velocity);
				appearedBody.rotation = stashedBody.rotation;
				appearedBody.angularVel = stashedBody.angularVel;
			}
			
		}
		
		public function setupTentacle():void
		{
			velcros.clear();
			
			var segmentObject:SegmentObjectNape = new SegmentObjectNape 
			(
				Game.game.space,
				Starling.contentScaleFactor,
				new Rectangle (250, 750, 140, 11),
				14
			);
			
			var indexData:Vector.<uint> = new Vector.<uint> ();
			
			for (var i:int = 0; i < segmentObject.segments.length * 2; i++) indexData.push (i , i + 1, i + 2 );
			
			for (i = 0; i < segmentObject.segments.length; i++) segmentObject.segments[i].body.userData.clingAble = true;
			
			tentacle = new SoftBodyNape(segmentObject, indexData, Texture.fromBitmap(new tentacleTexture(), false), 0xFFFFFF * Math.random(), false);
		}
		
		public function activateTentacle(b:Body, pos:Vec2, mishit:Boolean = false):void
		{
			tentacle.activate(b, pos, mishit, true);
			Game.game.layer3.addChild(tentacle);
		}
		
		public function deactivateTentacle():void
		{
			tentacle.deactivate(compoundBody.castCompound.bodies.at(7));
			Game.game.layer3.removeChild(tentacle);
		}
		
		public function setupArmor(gr:Image):void
		{
			armor = new Body();
			armor.shapes.add(new Circle(40, null, Material.steel()));
			armor.gravMass *= 3;
			armor.userData.graphic = gr;
			armor.cbTypes.add(Game.game.CB_HERO);
		}
		
		public function activateArmor():void
		{
			clearSoftBody();
			
			armor.space = Game.game.space;
			armor.position.set(center);
			center = armor.position;
			Game.game.layer3.addChild(armor.userData.graphic);
			state = ARMOR;
		}
		
		public function turn(dir:int):void
		{
			if (direction != dir)
			{
				direction = dir;
				force /= 2;
			}
			
			if (dir == 0)
			{
				if (!rockAble) rockAble = true;
				return;
			}
			
			if (clingConstr.space != null) 
			{
				rock(dir);
			}
		}
		
		private function uVec2SetBottom():void
		{
			uVec2.set(center);
			
			compoundBody.visitBodies(function(b:Body):void
			{
				if (b.position.y > uVec2.y) uVec2.set(b.position);
			});
		}
		
		public function checkImpulses(interactedBody:Body = null):Number
		{
			var i:Number = 0;
			compoundBody.visitBodies(function(b:Body):void
			{
				i += b.totalContactsImpulse(interactedBody).length;
			});
			return i;
		}
		
		public function checkDestrImpulses(interactedBody:Body, maxIpulses:uint = 2100):void
		{
			var impulses:Number = checkImpulses(interactedBody);
			
			if (impulses > maxIpulses)
			{
				setTimeout(function():void
				{
					clearSoftBody();
				}, 200);
				
				Game.game.gameOver(false, 1);
			}
		}
		
		private function runner():void
		{
			if (looseControllTime > 0) 
			{
				looseControllTime--;
				return;
			}
			
			if (ballEnabe > 0) 
			{
				ballEnabe --;
			}
			
			if (catchAble > 0) 
			{
				catchAble--;
			}
			
			if (rockTimer > 0) 
			{
				rockTimer--;
			}
			
			if (tentacle.active) 
			{
				tentacle.update();
			}
			
			if (state == BALL && sb.softPolygon == sb.ball)
			{
				if (center.y  > postPosition.y && center.y - postPosition.y > .5) 
				{
					uVec2.setxy(0, -4);
					compoundBody.visitBodies(function(b:Body):void
					{
						if (b.position.y > center.y)
						{
							b.applyImpulse(uVec2);
						}
					});
				}
				return;
			}
			
			if (clingConstr.space != null) 
			{
				return;
			}
			
			if (direction == 0)
			{
				force = 7;
				compoundBody.visitBodies(function(b:Body):void
				{
					b.velocity.x *= .7;
					if (!afterJump && b.velocity.y < -120)
					{
						b.velocity.y *= .7;
					}
				});
				
				return;
			}
			
			if (postPosition.x - center.x < 70 && force < FORCE_LIM) 
			{
				force += .35;
			}
			
			
			uVec2SetBottom();
			uVec2.y -= 7;
			uRay.origin.set(uVec2);
			uRay.maxDistance = RADIUS + RADIUS / 4;
			uRay.direction.setxy(1 * direction, 0);
			var res:RayResult = Game.game.space.rayCast(uRay, true, uFilter);
			
			if (res != null)
			{
				var ab:Body = null;
				var cBody:Body = res.shape.body;
				
				if (cBody.type == BodyType.DYNAMIC && !cBody.userData.dontMove && cBody.userData.link == null && cBody.allowMovement && center.y >= cBody.position.y) 
				{
					ab = cBody;
				}
			}
			
			var lim:uint = limVelocityGround;
			
			if (!onGround) 
			{
				lim = limVelocityAir;
			}
			else if (afterJump && catchAble == 0) 
			{
				afterJump = false;
			}
			
			compoundBody.visitBodies(function(b:Body):void
			{
				if (ab != null)
				{
					b.velocity.y = 21;
					b.velocity.x /= 2;
					uVec2.setxy(ab.gravMass / 4 * direction, -ab.gravMass / 2);
					ab.applyImpulse(uVec2);
				}
				
				if (Math.abs(b.velocity.x) > lim) 
				{
					b.velocity.x *= .8
				}
				else if (b.position.y < center.y)
				{
					var sqf:uint = 0;
					
					if (sqForce > 0)
					{
						sqf = 10;
					}
					uVec2.setxy(force * direction, sqf);
					
					b.applyImpulse(uVec2);
				}
			});
		}
		
		public function slowDown():void
		{
			force *= .9;
		}
		
		public function armored():void
		{
			if (direction == 0)
			{
				if (!onGround)
				{
					armor.velocity.x *= .95;
					armor.angularVel *= .95;
					return;
				}
				armor.velocity.x *= .8;
				armor.angularVel *= .8;
				return;
			}
			
			if (Math.abs(armor.velocity.x) > limVelocityGround * 2.1) 
			{
				armor.velocity.x *= .8;
				armor.angularVel *= .8;
			}
			
			uVec2.setxy(center.x, center.y - 35);
			armor.applyImpulse(Vec2.get(2100 * direction, 0, true), uVec2);
		}
		
		public function run():void
		{
			if (dieTimer > 0) 
			{
				dieTimer--;
				
				if (dieTimer == 0)  
				{
					clearSoftBody();
				}
				
				return;
			}
			
			if (bornTimer > 0) 
			{
				bornTimer--;
				
				if (dieTimer == 0)
				{
					activateSoftBody();
				}
				
				return;
			}
			
			if (jumpAbleTime > 0) 
			{
				jumpAbleTime--;
			}
			
			if (nextJumpTime > 0) 
			{
				nextJumpTime--;
			}
			
			switch(state)
			{
				case DEFAULT:
				case TENTACLE:
				case BALL:
					runner(); 
					break;
				case FLY: 
					flyer(); 
					break;
				case ARMOR: 
					armored();
			}
			
			postPosition.set(center);
		}
		
		public function rock(dir:int):void
		{
			if (!rockAble) 
			{
				return;
			}
			
			if ((dir * clingBody.velocity.x > 0 || Math.abs(clingBody.velocity.x) < 10) && rockTimer == 0) 
			{	
				uVec2.setxy(40 * dir, 0);
				
				compoundBody.visitBodies(function(b:Body):void
				{ 
					b.applyImpulse(uVec2);
				});
				rockAble = false;
				rockTimer = 70;
			}
			
		}
		
		private function getNearestVelcro():Body
		{
			var nv:Body = velcros.at(0);
			var dist:Number = Mut.distV(nv.position, center);
			
			velcros.foreach(function(b:Body):void
			{
				var d:Number = Mut.distV(b.position, center);
				if (d < dist)
				{
					nv = b;
					dist = d;
				}
			});
			
			return nv;
		}
		
		private function getClingBody():Body
		{
			var clingBody:Body = compoundBody.bodies.at(0);
			
			compoundBody.visitBodies(function(b:Body):void
			{
				if (b.position.y < center.y)
				{
					if (clingBody.position.y > b.position.y) 
					{
						clingBody = b;
					}
				}
			});
			
			return clingBody;
		}
		
		private function getChainBody(clingBody:Body):Body
		{
			var bodies:BodyList = Game.game.space.bodiesInAABB(new AABB(center.x - (RADIUS + 14), clingBody.position.y - 14, (RADIUS + 14) * 2, 28), false, true, uFilter);
			var chainBody:Body = null;
			var defineTop:Boolean;
			
			clingBody.velocity.y < 0 ? defineTop = true:defineTop = false;
			
			while (bodies.length > 0)
			{
				var tb:Body = bodies.pop();
				if (tb.type == BodyType.STATIC || !tb.userData.clingAble) continue;
				
				if ( chainBody == null || (chainBody.position.y < tb.position.y && !defineTop) || (chainBody.position.y > tb.position.y && defineTop) ) chainBody = tb;
			}
			
			return chainBody;
		}
		
		public function cling():Boolean
		{
			var tempCling:Body = getClingBody();
			var tempChain:Body = getChainBody(tempCling);
			
			if (tempChain != null)
			{
				clingBody = tempCling;
				chainBody = tempChain;
			}
			
			if (chainBody == null && state != TENTACLE) 
			{
				//clingBody = null;
				return false;
			}
			
			if(state == TENTACLE && chainBody == null)
			{
				var nearestVelcro:Body = getNearestVelcro();
				
				chainBody = tentacle.mSegmentObj.segments[tentacle.mSegmentObj.segments.length - 1].body;
				
				clingBody = compoundBody.bodies.at(0);
				var dist:Number = Mut.distV(clingBody.position, nearestVelcro.position);
				
				compoundBody.visitBodies(function(b:Body):void
				{
					var tempDist:Number = Mut.distV(b.position, nearestVelcro.position);
					if (tempDist < dist)
					{
						clingBody = b;
						dist = tempDist;
					}
				});
				
				sb.softPolygon.changeStiffness(true);
				
				var mishit:Boolean = false;
				
				if (Mut.distV(nearestVelcro.position, clingBody.position) > 210)
				{
					mishit = true;
				}
				
				activateTentacle(nearestVelcro, clingBody.position, mishit);
			}
			
			clingConstr.body1 = clingBody;
			clingConstr.body2 = chainBody;
			
			if (clingConstr.body2.userData.replaced != null) 
			{
				clingConstr.body2 = clingConstr.body2.userData.replaced;
			}
			
			uVec2.setxy(0, 0);
			clingConstr.anchor1.set(uVec2);
			
			if (chainBody.userData.clingOffset)
			{
				uVec2.set(chainBody.userData.clingOffset); 
			}
			
			clingConstr.anchor2.set(uVec2);
			
			clingConstr.space = Game.game.space;
			compoundBody.userData.pressureMul = 0;
			
			return true;
			
			
			
			
			
			/*if (clingBody == null)
			{
				clingBody = getClingBody();
				chainBody = getChainBody();
				
				if (chainBody == null && state != TENTACLE) 
				{
					clingBody = null;
					return false;
				}
				
				if(state == TENTACLE && chainBody == null)
				{
					var nearestVelcro:Body = getNearestVelcro();
					
					chainBody = tentacle.mSegmentObj.segments[tentacle.mSegmentObj.segments.length - 1].body;
					
					clingBody = compoundBody.bodies.at(0);
					var dist:Number = Mut.distV(clingBody.position, nearestVelcro.position);
					
					compoundBody.visitBodies(function(b:Body):void
					{
						var tempDist:Number = Mut.distV(b.position, nearestVelcro.position);
						if (tempDist < dist)
						{
							clingBody = b;
							dist = tempDist;
						}
					});
					
					sb.softPolygon.changeStiffness(true);
					
					var mishit:Boolean = false;
					
					if (Mut.distV(nearestVelcro.position, clingBody.position) > 210)
					{
						mishit = true;
					}
					
					activateTentacle(nearestVelcro, clingBody.position, mishit);
				}
			}
			
			clingConstr.body1 = clingBody;
			clingConstr.body2 = chainBody;
			
			if (clingConstr.body2.userData.replaced != null) 
			{
				clingConstr.body2 = clingConstr.body2.userData.replaced;
			}
			
			uVec2.setxy(0, 0);
			clingConstr.anchor1.set(uVec2);
			
			if (chainBody.userData.clingOffset)
			{
				uVec2.set(chainBody.userData.clingOffset); 
			}
			
			clingConstr.anchor2.set(uVec2);
			
			clingConstr.space = Game.game.space;
			compoundBody.userData.pressureMul = 0;
			
			return true;*/
		}
		
		public function removeCling(dontJump:Boolean = false):void
		{
			if (state != TENTACLE || tentacle.active || tentacle.mSegmentObj.isAdherent)
			{
				if (clingConstr.space != null) 
				{
					clingConstr.space = null;
					clingBody = null;
					chainBody = null;
					
					compoundBody.userData.pressureMul = 1;
					
					catchAble = 0;
					afterJump = true;
					
					if (tentacle.active)
					{
						sb.softPolygon.changeStiffness(false);
						deactivateTentacle();
					}
				}
			}
			
			if (dontJump)
			{
				return;
			}
			
			compoundBody.visitBodies(function(b:Body):void 
			{
				uVec2.setxy(jumpForce / 3 * direction, -jumpForce);
				b.applyImpulse(uVec2);
			});
		}
		
		private function checkGround():Boolean
		{
			var og:Boolean = false;
			
			compoundBody.visitBodies(function(b:Body):void 
			{
				if (b.userData.interact && b.position.y > center.y) 
				{
					og = true;
					return;
				}
			});
			
			return og;
		}
		
		public function jump():void
		{
			if (state == BALL && sb.softPolygon == sb.ball)
			{
				return;
			}
			
			if (state == ARMOR)
			{
				onGround = false;
				jumpAbleTime = 7;
				
				uVec2.setxy(0, -35000);
				armor.applyImpulse(uVec2);
				return;
			}
			
			if (clingConstr.space != null) 
			{
				if (state != TENTACLE || tentacle.mSegmentObj.isAdherent)
				{
					removeCling();
				}
				
				return;
			}
			
			catchAble = 7;
			
			if (state == TENTACLE)
			{
				cling();
			}
			else
			{
				if (!cling())
				{
					if (jumpAbleTime > 0 || onGround)
					{
						_jump();
					}
				}
			}
		}
		
		private function _jump():void
		{
			if (nextJumpTime != 0)
			{
				return;
			}
			
			nextJumpTime = 7;
			
			onGround = false;
			afterJump = true;
			jumpAbleTime = 0;
			
			uVec2.setxy(0, -jumpForce);
			applyForce(uVec2);
			
			/*compoundBody.visitBodies(function(b:Body):void 
			{
				uVec2.setxy(0, -jumpForce);
				b.applyImpulse(uVec2);
			});*/
		}
		
		public function applyForce(force:Vec2):void
		{
			compoundBody.visitBodies(function(b:Body):void 
			{
				b.applyImpulse(force);
			});
		}
		
		public function applyVel(vel:Vec2):void
		{
			compoundBody.visitBodies(function(b:Body):void 
			{
				b.velocity.set(vel);
			});
		}
		
		//___FLY__________________________________________________________
		public function fly():void
		{
			var a:Number = 1;
			if (flyBody.velocity.y > 0) a = 1.4;
			uVec2.setxy(0, -jumpForce * 2.1 * a);
			flyBody.applyImpulse(uVec2);
		}
		
		private function flyer():void
		{
			var a:Number = 1;
			if (flyBody.velocity.x * direction < 0) a = 2.8;
			
			if (direction == 0)
			{
				compoundBody.visitBodies(function(b:Body):void 
				{
					b.velocity.x *= .997;
				});
				return;
			}
			
			uVec2.setxy(14 * direction * a, 0);
			flyBody.applyImpulse(uVec2);
		}
		
		//CLEAR___________________________________________________________
		public function clearSoftBody():void
		{
			sb.noRender = true;
			sb.softPolygon.body.space = null;
		}
		
		public function activateSoftBody(pos:Vec2 = null):void
		{
			sb.noRender = false;
			
			if (pos != null) 
			{
				sb.softPolygon.body.visitBodies(function(b:Body):void
				{
					b.position.setxy(pos.x + b.position.x - center.x, pos.y + b.position.y - center.y);
				});
			}
			
			sb.softPolygon.body.space = Game.game.space;
		}
		
		public function dying(dieCase:uint = 0):void
		{
			if (isDead)
			{
				return;
			}
			
			isDead = true;
			
			switch(state)
			{
				case 0:
				case 1:
					carmaLife = 0;
					clearSoftBody();
					break;
				case 2:
					armor.space = null;
					armor.userData.graphic.parent.removeChild(armor.userData.graphic);
			}
		}
		
		public function mark():void
		{
			uVec2.y -= 7;
			uRay.origin.set(uVec2);
			uRay.maxDistance = RADIUS + RADIUS / 4;
			uRay.direction.setxy(1 * direction, 0);
			var res:RayResult = Game.game.space.rayCast(uRay, true, uFilter);
			
			if (res != null)
			{
				var ab:Body = null;
				var cBody:Body = res.shape.body;
				if (cBody.type == BodyType.DYNAMIC && !cBody.userData.dontMove && cBody.userData.link == null && cBody.allowMovement && center.y >= cBody.position.y) ab = cBody;
			}
		}
	}
}